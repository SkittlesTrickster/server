package server;

import gamePlay.RoomHandler;
import gamePlay.Use;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

import databaseFunctions.MainMenuFunctions;
import databaseFunctions.PlayerHandling;
import databaseFunctions.RoomFunctions;


public class Receiver{
	//Boolean running = true;
	private static final int PORT = 9001;
	// Initiate ph first for chatThread
	static PlayerHandling ph = new PlayerHandling();

	// initiate chat thread
	static ChatThread chatThread = new ChatThread(ph);

	// Initiate all Database classes here.
	static MainMenuFunctions mmf = new MainMenuFunctions();

	static RoomFunctions rf = new RoomFunctions();

	public Receiver(){

		try {
			initServ();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void initServ() throws Exception{
		System.out.println("The Reciever is running.");
		ServerSocket listener = new ServerSocket(PORT);
		try {
			while (true) {
				new Handler(listener.accept()).start();
			}
		} finally {
			listener.close();
		}
	}

	private static class Handler extends Thread {
		RoomHandler room;
		Use use;


		//private String receivedData;
		private Socket socket;
		private BufferedReader in;
		private PrintWriter out;

		/**
		 * Constructs a handler thread, squirreling away the socket.
		 * All the interesting work is done in the run method.
		 */
		public Handler(Socket socket) {
			this.socket = socket;
		}

		public void run() {
			try {
				String prefix = "";

				// Create character streams for the socket.
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				// initiate Input Handler
				InputHandler i = new InputHandler();
				// initiate Use
				use = new Use(out,rf,ph,i);
				// initiate RoomHandler
				room = new RoomHandler(out,ph,rf,use,i);

				//chatThread.addPlayer(out);

				Boolean runningX = true;
				// First message sent to the Client used to be here

				// Init Game Thread
				GameThread gameThread = new GameThread(out, mmf, ph, rf, room, use, chatThread, i);
				gameThread.start();

				// Accept messages from this client
				while (runningX) {
					if(!in.equals(null)){
						// Receive input
						if(in.ready()){
							String input = in.readLine();
							if(input != ""){
								System.out.println("Recieved: " + input);
								// Process input
								// ------------------------------------------------------
								if(input.startsWith("CHAT ")){
									String text = input.substring(5);
									if(prefix.equals("")){
										String handle = ph.getHandle(room.pid);
										// split handle
										String[] r = handle.split("(?=\\p{Upper})");
										prefix = "C" + r[0].substring(0,1) + r[1].substring(0,1);
										prefix = prefix.toUpperCase() + ": ";
									}
									// send to chat
									chatThread.recieveMessage(prefix + text);
								}
								// ------------------------------------------------------
								else if(input.startsWith("GAME ")){
									// send to game
									//gameThread.recieveMessage(input.substring(5));
									i.setInput(input.substring(5));
								}
								// ------------------------------------------------------
								else if(input.equals("END")){
									runningX = false;
									socket.close();
								}
								// ------------------------------------------------------
								else{
									System.out.println("Received Strange Message: " + input);
								}
								// ------------------------------------------------------
							}
						}
						try {
							TimeUnit.MILLISECONDS.sleep(100);
						} catch (InterruptedException e) {
							System.out.println("Sleep failed in Receiver thread");
							e.printStackTrace();
						}
					}else{
						runningX = false;
						socket.close();
					}
				}
			} catch (IOException e) {
				System.out.println(e);
			} finally {
				// Socket Closing - Cleanup
				System.out.println("Finally Triggered.");
				if (out != null) {
					// Set Pid
					int pid = room.pid;
					//int sid = room.sid; //Use with chat later
					// Remove Player from Chat
					chatThread.removePlayer(pid, ph.getHandle(pid));
					// Stop Meteor Countdown Timer
					use.stopTimer();
					//
					room.closeAllRoomLoops();
				}
				// Close socket
				try {
					socket.close();
				} catch (IOException e) {
				}
			}
		}

		@SuppressWarnings("unused")
		public static void main(String[] args) {
			new Receiver();
		}

	}
}
