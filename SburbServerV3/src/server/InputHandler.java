package server;

import java.util.concurrent.TimeUnit;

public class InputHandler {
	String input = "";
	
	public void setInput(String s){
		input = s;
	}
	
	
	public String waitForInput(){
		String str = "";
		Boolean lookingForInput = true;
		while(lookingForInput){
			if(input.equals("")){
				try {
					//System.out.println("Sleep 10ms");
					TimeUnit.MILLISECONDS.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else{
				System.out.println("GameInputRecognized: " + input);
				str = input;
				lookingForInput = false;
				input = "";
			}
		}
		System.out.println("WaitForInput Returning: "+ str);
		return str;		
	}
}
