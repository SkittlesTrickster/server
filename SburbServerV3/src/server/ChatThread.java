package server;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import databaseFunctions.PlayerHandling;

public class ChatThread{
	Sendificator send = new Sendificator();
	PlayerHandling ph;
	private static Boolean sendificatorRunning = false;
	private static HashSet<PrintWriter> writers = new HashSet<PrintWriter>();
	private static ArrayList<String> stringsToBroadcast = new ArrayList<String>();
	private static ArrayList<Integer> onlinePlayers = new ArrayList<Integer>();
	//private static Map<Integer, ArrayList<PrintWriter>> sessionWriters = new HashMap<Integer, ArrayList<PrintWriter>>();
	private Map<Integer, PrintWriter> privWriters = new HashMap<Integer, PrintWriter>();
	PrintWriter tempWriter;

	public ChatThread(PlayerHandling p){
		ph = p;
		send.start();
	}

	public void addPlayer(int pid, String handle, PrintWriter w){
		onlinePlayers.add(pid);
		writers.add(w);
		privWriters.put(pid, w);
		// Add Player to all online player's lists and notify that they are online
		for (PrintWriter op : writers) {
			op.println("CHAT LIST+ " + handle);
			op.println("CHAT DISP -- " + handle + " is online --");
		}
		PrintWriter thisPlayer = privWriters.get(pid);
		for (int op : onlinePlayers){
			// send username
			thisPlayer.println("CHAT LIST+ " + ph.getHandle(op));
			System.out.println("CHAT LIST+ " + ph.getHandle(op));
		}
	}
	
	public void removePlayer(int pid, String handle){
		System.out.println("** removing " + pid);
		onlinePlayers.remove(onlinePlayers.indexOf(pid));
		for (PrintWriter op : writers) {
			op.println("CHAT LIST- " + handle);
			op.println("CHAT DISP -- " + handle + " went offline --");
		}
	}

	public Boolean isOnline(int pid){
		Boolean online = true;
		if (!onlinePlayers.contains(pid)){
			online = false;
		}
		System.out.println("Players online:");
		for(int x : onlinePlayers){
			System.out.println(x);
		}
		System.out.println("-- End --");
		return online;
	}

	public void recieveMessage(String message) {
		System.out.println("Chat Thread Triggered: " + message);
		stringsToBroadcast.add(message);
		for(PrintWriter o : writers){
			System.out.println("Sent to Client: CHAT DISP ChatMess: " + message);
			o.println("CHAT DISP " + message);
			o.flush();
		}

	}

	private static class Sendificator extends Thread {
		public void run(){
			System.out.println("Start sendificator");
			while(sendificatorRunning){
				//System.out.println("run");
				while(!stringsToBroadcast.isEmpty()){
					System.out.println("Sendificator loop");
					String message = stringsToBroadcast.get(0);
					for(PrintWriter op : writers){
						System.out.println("writer: " + op);
						System.out.println("message: " + message);
						op.print("CHAT DISP ChatMess: " + message);
						op.flush();
					}	
					stringsToBroadcast.remove(0);
				}
				rest100();
			}
		}

		public void rest100(){
			try {
				TimeUnit.MILLISECONDS.sleep(100);
			} catch (InterruptedException e) {
				System.out.println("Sleep failed in Chat thread");
				e.printStackTrace();
			}
		}
	}

}
