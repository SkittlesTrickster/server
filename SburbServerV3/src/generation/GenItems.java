package generation;

import java.util.ArrayList;
import java.util.Random;

import databaseFunctions.MapFunctions;

/**
 * Generates a list of Items based on a maptype
 * Acts as a go-between for MapFunctions so that getItemID isn't backlogged
 * @author Rena
 *
 */
public class GenItems {
	MapFunctions mf = new MapFunctions();
	Random rand = new Random();
	private ArrayList<Integer> IDlist = new ArrayList<Integer>();
	private int size = -1;
	
	public GenItems(MapFunctions mm){
		mf = mm;
	}
	
	public int getSize(){return size;}
	
	public int getItemID(String focusItem){
		int id = mf.getItemID(focusItem);
		return id;
	}

	private void generateRandomItems() {
		ArrayList<Integer> listOfX_items = new ArrayList<Integer>(); // contains IDs
		int numItems = rand.nextInt(6);
		for(int i = 0; i <= numItems;){
			listOfX_items.add(IDlist.get(rand.nextInt(IDlist.size())));
			i++;
		}
	}
	
	public ArrayList<Integer> getIDlist(String type){
		IDlist = mf.getIDlist(type);
		size = IDlist.size();
		return IDlist;
	}
	
	public int getID(int x){
		return IDlist.get(x);
	}
}
