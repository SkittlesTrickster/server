package generation;

import java.awt.Color;
import java.util.Random;

public class PlanetGen {
	Random rand = new Random();

	//// LAND NAME
	String[] breathLand = {"Wind", "Haze", "Smoke", "Flow", "Zephyr", "Helium", "Signs", "Showers", "Fog", "Whistling", "Thunder", "Steam",
			"Rivers", "Levitation", "Highways", "Clouds", "Suffocation", "Kites", "Compasses", "Tubes", "Lighthouses"};
	String[] heartLand = {"Tea", "Krypton", "Sorrow", "Reflections", "Mirrors", "Happiness", "Milk", "Dairy", "Memories", "Ships", "Spirits",
			"Gaiety", "Lakes", "Chrome", "Cards", "Bridges", "Doors", "Dolls", "Vessels", "Windows", "Festivals", "Waterfalls", "Song"};
	String[] hopeLand = {"Angels", "Feathers", "Mounds", "Xenon", "Balloons", "Cathedrals", "Bells", "Silver", "Dreams", "Miracles", "Genesis",
			"Ladders", "Glitter", "Oasis", "Lanterns", "Piety", "Unicorns", "Crosses", "Flags", "Trumphets" };
	String[] doomLand = {"Triangles", "Crypts", "Death", "Tombs", "Electronics", "Robots", "Inevitability", "Desolation", "Stairs", "Bones", 
			"Dust", "Circuitry", "Endings", "Stone", "Curses", "Drought", "Law", "Legends", "Duality", "Zombies"};
	String[] timeLand = {"Gears", "Heat", "Quartz", "Melody", "Clockwork", "Frequency", "Clocks", "Sand", "Radios", "Movement", "Anticity",
			"Fossils", "Artifacts", "Ash", "Graves", "Wastelands", "Echoes", "Magma", "Hourglasses", "Ticking", "Cogs"};
	String[] mindLand = {"Brains", "Illusions", "Thoughts", "Peace", "Books", "Coins", "Traps", "Keys", "Deja-vu", "Labyrinth", "Knowledge",
			"Locks", "Chemicals", "Enigmas", "Math", "Spoons", "Crossroads", "Puzzles", "Mystery", "Obstacles", "Spheres", "Dominos"};
	String[] lightLand = {"Rainbows", "Sunshine", "Rays", "Maps", "Fireworks", "Journeys", "Glow", "Paint", "Colors", "Vision", "Stars", 
			"Talismans", "Lazers", "Art", "Ultraviolet", "Beauty", "Bioluminescence", "Clairity", "Contrast", "Prisms"};
	String[] rageLand = {"Mirth", "Tents", "Wrath", "Explosions", "Therapy", "Ruins", "Turbulence", "Distortion", "Misdirection", "Repitition",
			"Geysers", "Lightning", "Volcanoes", "Radiation", "Maelstrom", "Chaos", "Beasts", "Earthquakes", "Poison", "Acid", "Danger", 
			"Terror", "Rubble", "Warriors"};
	String[] voidLand = {"Shade", "Cubes", "Void", "Holes", "Rifts", "Shadows", "Pumpkins", "Null", "Silence", "Obsidian", "Emptiness", "Ink", 
			"Static", "Obscurity", "Snow", "Silhouettes", "Amnesia", "Gaps", "Freefall", "Statues"};
	String[] bloodLand = {"Pulse", "Branches", "Chains", "Iron", "Bindings", "Clay", "Promises", "Copper", "Steel", "Plasma", "Ropes", "Alignment",
			"Ooze", "Gravity", "Tunnels", "Knots", "Organs", "Tissue", "Shackles", "Combination"};
	String[] lifeLand = {"Flowers", "Forest", "Dew", "Mushrooms", "Vines", "Grass", "Trees", "Springs", "Fungi", "Roses", "Sap", "Corn", "Thorns",
			"Cacti", "Farms", "Moss", "Sickness", "Aquariums", "Soil", "Growth", "Gardens", "Swamps", "Jungles", "Bamboo", "Evergreens", "Meadows",
			"Leaves", "Coral", "Slime"};
	String[] etcLandArray = {"Caves", "Treasure", "Little Cubes", "Glass", "Pyramids", "Neon", "Krypton", "Junk", "Cliffs", "Anime Shades", "Change",
			"Dragons", "Frost", "Repetition", "Disorder", "Order", "Magnetite", "Glitches", "Paper", "Wells", "Water", "Words", "Chalk", "Boxes",
			"Jade", "Emerald", "Pearl", "Ruby", "Sapphire", "Rain", "Drums", "Gum", "Towers", "Machines", "Merry-Go-Rounds", "Meandering", "Salt",
			"Trampolines", "Islands", "Factories", "Bounciness", "Demons", "Helixes", "Battle", "Plinko", "Chessmen", "Levitation", "Methane", 
			"Fire", "Spikes", "Lollipops", "Candy", "Castles", "Surveillance", "Bubbles", "Storms", "Doldrums", "Expanse", "Fabric", "Materials",
			"Divides", "Stardust", "Night", "Midnight", "Portals", "Grandeur", "Minitures", "Bees", "Rooftops", "Statues", "Gargoyles", "Jello",
			"Puppets", "Amusement", "Sponges", "Insomnia", "Walls", "Canyons", "Crystal", "Hail", "Velvet", "Junk", "Ivory", "Butterflies",
			"Cages", "Coal", "Cobblestone", "Plains", "Spires", "Carnage", "Silk"};
	
	//// COLOR ARRAYS
	// Low Alt to High Alt // one too many colors
	// 0 is water, 1(Shore), 2, 3(Mid), 4, 5(Peaks)
	Color[] redLand = {new Color(0xFF9999),new Color(0xCC6666),new Color(0xFF3333) ,new Color(0xCC0000),new Color(0x990000),new Color(0x660000)};
	Color[] blueLand = {new Color(0x6666CC),new Color(0x3333CC),new Color(0x0000CC) ,new Color(0x000099),new Color(0x000066),new Color(0x000033)};
	Color[] greenLand = {new Color(0x66FF99),new Color(0x33CC66),new Color(0x009933) ,new Color(0x006600),new Color(0x336633),new Color(0x003300)};
	Color[] yellowLand = {new Color(0xFFFFCC),new Color(0xFFFF66),new Color(0xFFFF33) ,new Color(0xCCCC00),new Color(0xCCCC66),new Color(0xCCCC99)};
	Color[] orangeLand = {new Color(0xFFCC66),new Color(0xFF9966),new Color(0xFF9933) ,new Color(0xFF6633),new Color(0xFF3300),new Color(0xCC3300)};
	Color[] blackLand = {new Color(0xCCCCCC),new Color(0x757575),new Color(0x525252) ,new Color(0x3B3B3B),new Color(0x2B2B2B),new Color(0x1A1A1A)};
	Color[] whiteLand = {new Color(0xCCFFFF),new Color(0xFFFFCC),new Color(0xFFFFFF) ,new Color(0xF0F0F0),new Color(0xDEDEDE),new Color(0xD3D3D3)};
	Color[] greyLand = {new Color(0xAAAAAA),new Color(0xA3A3A3),new Color(0x949494) ,new Color(0x858585),new Color(0x7A7A7A),new Color(0x666666)};
	Color[] tealLand = {new Color(0x99CCCC),new Color(0x66cccc),new Color(0x3CCCC) ,new Color(0x339999),new Color(0x009999),new Color(0x006666)};
	Color[] brownLand = {new Color(0xffcc99),new Color(0xcc9966),new Color(0x996666) ,new Color(0x996633),new Color(0xcc6600),new Color(0x663300)};
	Color[] purpleLand = {new Color(0xcc99ff),new Color(0x9966cc),new Color(0x9933FF) ,new Color(0x663399),new Color(0x6600cc),new Color(0x330066)};
	Color[] pinkLand = {new Color(0xFFCCFF),new Color(0xFF99FF),new Color(0xFF66FF) ,new Color(0xFF33FF),new Color(0xCC00CC),new Color(0x990099)};

	//// CONSORTS
	String[] consortColor = {"Red","Blue","Green","Black","White","Yellow","Purple","Brown","Orange","Teal","Cyan",
			"Pink","Grey"}; //24
	// Limit animals to animals with "hands"
	String[] consortAnimal = {"Iguana","Salamander","Crocodile","Turtle","Cat","Dog","Jellyfish","Mouse","Ferret",
			"Robot","Racoon","Squirrel"};
	
	///// DENIZEN
	String[] denizenName = {"Typheus", "", "Abraxas", "", "Hephaestus", "Echidna", "", "Cetus", "", "Nyx", "", "Hemera"}; // Blood, Rage, Mind, Doom, Heart

	public String genConsort(){
		return consortColor[rand.nextInt(consortColor.length)] +" "+ consortAnimal[rand.nextInt(consortAnimal.length)];
	}
	
	public String genLand01(String aspect){
		// aspect specific
		String land01 = "";
		if(aspect.equals("Space")){
			land01 = randAny();
		}
		else{
			land01 = randByAspect(aspect);
		}
		return land01;
	}

	public String genLand02(String aspect){
		String land02 = "";
		if(aspect.equals("Space")){
			land02 = "Frogs";
		}
		else{
			land02 = randAny();
		}
		return land02;
	}

	public String randAny(){

		int whichAspect = rand.nextInt(12); //between 0 and specified variable so 12 aspect arrays 0-11
		String[] arrayHolder = null;

		if (whichAspect == 0){
			arrayHolder = breathLand;
		}
		if (whichAspect == 1){
			arrayHolder = heartLand;
		}
		if (whichAspect == 2){
			arrayHolder = hopeLand;
		}
		if (whichAspect == 3){
			arrayHolder = doomLand;
		}
		if (whichAspect == 4){
			arrayHolder = timeLand;
		}
		if (whichAspect == 5){
			arrayHolder = mindLand;
		}
		if (whichAspect == 6){
			arrayHolder = lightLand;
		}
		if (whichAspect == 7){
			arrayHolder = rageLand;
		}
		if (whichAspect == 8){
			arrayHolder = voidLand;
		}
		if (whichAspect == 9){
			arrayHolder = bloodLand;
		}
		if (whichAspect == 10){
			arrayHolder = lifeLand;
		}
		if (whichAspect == 11){
			arrayHolder = etcLandArray;
		}

		if (arrayHolder != null){
			int whichLand = rand.nextInt(arrayHolder.length - 1);
			String landB = arrayHolder[whichLand];
			return landB;
		}
		else {
			System.out.println("Somebody Got a Null Land");
			return "Null";
		}
	}

	public String randByAspect(String aspect){
		String aLand = "";
		if(aspect.matches("Breath")){
			aLand = returnLand(breathLand);
		}
		if(aspect.matches("Hope")){
			aLand = returnLand(hopeLand);
		}
		if(aspect.matches("Heart")){
			aLand = returnLand(heartLand);
		}
		if(aspect.matches("Doom")){
			aLand = returnLand(doomLand);
		}
		if(aspect.matches("Time")){
			aLand = returnLand(timeLand);
		}
		if(aspect.matches("Mind")){
			aLand = returnLand(mindLand);
		}
		if(aspect.matches("Light")){
			aLand = returnLand(lightLand);
		}
		if(aspect.matches("Rage")){
			aLand = returnLand(rageLand);
		}
		if(aspect.matches("Void")){
			aLand = returnLand(voidLand);
		}
		if(aspect.matches("Blood")){
			aLand = returnLand(bloodLand);
		}
		if(aspect.matches("Life")){
			aLand = returnLand(lifeLand);
		}
		return aLand;
	}

	public String returnLand(String[] aspectArray){
		int temp = rand.nextInt(aspectArray.length - 1);
		String landT = aspectArray[temp];
		return landT;
	}
	

	public Color[] generateColors(){
		Color[] colors = {};
		int ic = rand.nextInt(12);
		if(ic == 0){
			colors = redLand;
			System.out.println(ic+ " red");
		}
		if(ic == 1){
			colors = blueLand;
			System.out.println(ic+ " blue");
		}
		if(ic == 2){
			colors = greenLand;
			System.out.println(ic+ " green");
		}
		if(ic == 3){
			colors = yellowLand;
			System.out.println(ic+ " yellow");
		}
		if(ic == 4){
			colors = orangeLand;
			System.out.println(ic+ " orange");
		}
		if(ic == 5){
			colors = blackLand;
			System.out.println(ic+ " black");
		}
		if(ic == 6){
			colors = whiteLand;
			System.out.println(ic+ " white");
		}
		if(ic == 7){
			colors = greyLand;
			System.out.println(ic+ " grey");
		}
		if(ic == 8){
			colors = tealLand;
			System.out.println(ic+ " teal");
		}
		if(ic == 9){
			colors = brownLand;
			System.out.println(ic+ " brown");
		}
		if(ic == 10){
			colors = purpleLand;
			System.out.println(ic+ " purple");
		}
		if(ic == 11){
			colors = pinkLand;
			System.out.println(ic+ " pink");
		}
		return colors;
	}
}
