package gamePlay;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import server.InputHandler;
import databaseFunctions.PlayerHandling;
import databaseFunctions.RoomFunctions;


public class RoomHandler {
	InputHandler i;
	RoomFunctions rf;
	PlayerHandling p;
	Use u;

	// Id Numbers
	public int pid, sid, mid;

	int inventoryCount = 0;

	// incoming text
	String input="";

	// Loop
	Boolean roomLoop = false;
	Boolean displayFlavorText = true;

	// Room Variables
	String location;
	int mapWidth;
	int mapHeight;
	String focusItem;
	String locToGo;
	int ty;
	int tx;

	// Room Variables
	int roomType = -1; // Defaults to No room
	ArrayList<String> leaveOptionsStr = new ArrayList<String>();
	ArrayList<String> arrayOfRoomItems = new ArrayList<String>(); // Strings
	String roomItemsStr = ""; // Room String

	// Read Write
	PrintWriter out;

	public RoomHandler(PrintWriter o, PlayerHandling pl, RoomFunctions r, Use use, InputHandler ip){
		out = o;
		p = pl;
		rf = r;
		u = use;
		i = ip;
	}

	public void changeLocation(String location){
		// Get MapID for new Location in This session
		System.out.println("Change Location SID: "+ sid);
		mid = rf.getMID(location, pid, sid);
		System.out.println("Change Location MID: " + mid);
		// Change Location in Database
		p.changePlayerLocation(location, pid);
		// Assign Room Variables
		assignRoomVariables(mid, location, p.getx(pid), p.gety(pid), rf.getMapWidth(mid), rf.getMapHeight(mid)); // Map in Dimensions
		// Send all Map Types so Map Can be Drawn
		out.println("UPDATE LOCATION " + rf.getMapWidth(mid)+ " "+ rf.getMapHeight(mid)+ " "+ rf.sendMapTypes(mid));
		// Update Yellow Square on Client Map
		String sendOut = "UPDATE ROOM " + tx + " " + ty;
		out.println(sendOut);
		System.out.println("Sent Update Location. Room: " + sendOut);
		// Start Map Loop and Display Flavor Text
		if(roomLoop == false){
			displayFlavorText = true;
			roomLoop();
		}
	}

	public void changeRoom(){
		// Assign Room Variables for New Room
		assignRoomVariables(mid, location, p.getx(pid), p.gety(pid), rf.getMapWidth(mid), rf.getMapHeight(mid));
		// Update Yellow Square on Client
		out.println("UPDATE ROOM " + tx + " " + ty);
		System.out.println("Sent Update Room");
	}

	public void assignRoomVariables(int MapID, String loc, int x, int y, int wid, int hei){
		// Set Room Variables
		location = loc;
		tx = x;
		ty = y;
		mapWidth = wid;
		mapHeight = hei;
		System.out.println("MapID: "+ MapID + " x: "+x+" y: "+y);
		roomType = getMapType(MapID,x,y);
		System.out.println("RoomType: " +roomType);
		getLeaveArray(MapID,x,y);
		//System.out.println("GetLeave Success");
		getRoomItemsArray(MapID,x,y);
		//System.out.println("GetItems Success");
	}

	private void roomLoop() {
		roomLoop = true;
			while(roomLoop){
				if(displayFlavorText){
					// Display Flavor Text
					if(location.contains("house")){
						if(tx == 0){
							out.println("GAME DISP You are in your bedroom.");
						}
						else if(tx == 1){
							out.println("GAME DISP You are in the bathroom.");
						}
						else if(tx == 2){
							out.println("GAME DISP You are in the upstairs hallway.");
						}
						else if(tx == 3){
							out.println("GAME DISP You are in the downstairs hallway.");
						}
						else if(tx == 4){
							out.println("GAME DISP You are in the livingroom.");
						}
						else if(tx == 5){
							out.println("GAME DISP You are in the kitchen.");
						}
						else if(tx == 6){
							out.println("GAME DISP You are in your parents' room.");
						}
						else if(tx == 7){
							out.println("GAME DISP You are in the attic.");
						}
						else if(tx == 8){
							out.println("GAME DISP You are on the roof.");
						}
						else if(tx == 9){
							out.println("GAME DISP You are in the front yard.");
						}
						else if(tx == 10){
							out.println("GAME DISP You are in the back yard.");
						}
						else if(tx == 11){
							out.println("GAME DISP You are in the garage.");
						}
					}
					else if(location.contains("planet")){
						out.println("GAME DISP You are on the land of "+ p.getPlanetName01(pid) +" and " + p.getPlanetName02(pid) );
					}
					else{
						out.println("GAME DISP You are in the " + location);
					}
					if(!location.contains("house")){
						displayFlavorText = false;
					}
				}
				// Clear focus variables
				focusItem = "";
				locToGo = "";

				// Update Room Variables
				assignRoomVariables(mid, location, p.getx(pid), p.gety(pid), rf.getMapWidth(mid), rf.getMapHeight(mid));

				// Display Items in room
				out.println("GAME DISP "+roomItemsStr);
				System.out.println("GAME DISP "+roomItemsStr);
				// Query Options
				mainMenu();
				// Get response
				input = i.waitForInput().toLowerCase(); // trim reply and lower case
				System.out.println("MainMenu Input:"+input);

				//// MAIN MENU CHECK
				if(input.startsWith("leave")){
					// Query leave options ... and Cancel
					out.println("GAME Q "+arrayToString(leaveOptionsStr)+"Cancel |");
					// Get locToGo
					locToGo = i.waitForInput().toLowerCase(); // trim leave
					System.out.println("LocToGo: "+ locToGo);
					System.out.println(leaveOptionsStr);
					// Check locToGo is available
					if(leaveOptionsStr.contains(locToGo)){
						String goHere = "";
						// Change location based on input
						if(locToGo.equals("north")){
							if(ty-1 >= 0){
								p.changeY(ty-1, pid);
							}
							else{
								p.changeY(mapHeight-1, pid);
							}
						}
						else if(locToGo.equals("south")){
							if(ty+1<mapHeight){
								p.changeY(ty+1, pid);
							}
							else{
								p.changeY(0, pid);
							}
						}
						else if(locToGo.equals("east")){
							if(tx-1 >= 0){
								p.changeX(tx-1, pid);
							}
							else{
								p.changeX(mapWidth-1, pid);
							}
						}
						else if(locToGo.equals("west")){
							if(tx+1 < mapWidth){
								p.changeX(tx+1, pid);
							}
							else{
								p.changeX(0, pid);
							}
						}
						else if(locToGo.equals("hallway")){
							// Upstairs Hallway
							if(tx == 0 || tx == 1 || tx == 7){
								p.changeX(2, pid);
							}
							// Downstairs Hallway
							if(tx == 4 || tx == 5 || tx == 6 || tx == 11){
								p.changeX(3, pid);	
							}
						}
						else if(locToGo.equals("bedroom")){
							p.changeX(0, pid);
						}
						else if(locToGo.equals("bathroom")){
							p.changeX(1, pid);
						}
						else if(locToGo.equals("upstairs")){
							p.changeX(2, pid);
						}
						else if(locToGo.equals("downstairs")){
							p.changeX(4, pid);
						}
						else if(locToGo.equals("livingroom")){
							p.changeX(4, pid);
						}
						else if(locToGo.equals("kitchen")){
							p.changeX(5, pid);
						}
						else if(locToGo.equals("parents room")){
							p.changeX(6, pid);
						}
						else if(locToGo.equals("attic")){
							p.changeX(7, pid);
						}
						else if(locToGo.equals("roof")){
							p.changeX(8, pid);
						}
						else if(locToGo.equals("front yard")){
							p.changeX(9, pid);
						}
						else if(locToGo.equals("back yard")){
							p.changeX(10, pid);
						}
						else if(locToGo.equals("garage")){
							p.changeX(11, pid);
						}
						else if(locToGo.equals("inside")){
							if(tx == 9){ // front
								p.changeX(4, pid);
							}
							if(tx == 10){ // back
								p.changeX(5, pid);
							}
						}
						else if(locToGo.equals("outside")){
							tx = p.getHouseX(pid);
							ty = p.getHouseY(pid);
							p.changeX(tx, pid);
							p.changeY(ty, pid);
							changeLocation(pid+"planet");
						}
						else if(locToGo.equals("house")){
							p.changeX(4, pid);
							p.changeY(0, pid);
							changeLocation("house");
						}
						else{
							// change player loc directly
							goHere = locToGo;
							//p.changePlayerLocation(goHere, pid);
						}

						// Restart Location
						System.out.println("Location: "+ location);
						System.out.println("Gohere: "+ goHere);
						System.out.println("Room Moved to: "+p.getx(pid)+" "+ p.gety(pid));

						if(goHere.equals("")){
							// If no new Location
							System.out.println("Change Room");
							changeRoom();
							System.out.println("Leave to " + locToGo);
						}
						else{
							// If new Location
							System.out.println("Change Location");
							changeLocation(goHere);
							System.out.println("Leave to " + goHere);
						}
						//roomLoop = false;
					}
					else{
						out.println("GAME DISP Location:" + locToGo + " not understood.");
					}

				}
				else if(input.startsWith("use ")){
					focusItem = input.substring(4);
					System.out.println("use:"+focusItem);
					u.useThing(focusItem,mid,tx,ty,arrayOfRoomItems);
				}
				else if(input.startsWith("c ")||input.startsWith("captcha ")){	
					// Assign focus
					if(input.startsWith("c ")){
						focusItem = input.substring(2);
					}
					else if(input.startsWith("captcha ")){
						focusItem = input.substring(8);
					}
					//System.out.println("Focus Item: "+focusItem);
					// Get Item
					if(arrayOfRoomItems.contains(focusItem)){
						if(rf.checkIfRemovableFromRoom(focusItem)){
							int inv = p.countInventory(pid);
							System.out.println("Inventory Size: " + inv);
							if(inv<10){ //Check that the inventory is not full
								// Get ID of focusItem
								int focusID = rf.getItemID(focusItem);
								// Add item to Inventory
								rf.addToInventory(pid, focusID);
								// Remove item from Map
								rf.removeFromRoom(mid, tx, ty, rf.getItemID(focusItem));
								//System.out.println("Inventory: "+stringOfItems);
								// Update GUI
								out.println("UPDATE INVENTORY "+ p.getPlayerInventoryString(pid));
								// getItems
								getRoomItemsArray(mid,tx,ty);
							}
							else{
								out.println("GAME DISP You must Decaptcha an item before you can Captcha another");
							}
						}
						else{
							out.println("GAME DISP You cannot captchalogue "+ focusItem + ".");
						}
					}
				}
				else if(input.startsWith("d ")||input.startsWith("decaptcha ")){	
					// Assign focus
					if(input.startsWith("d ")){
						focusItem = input.substring(2);
					}
					else if(input.startsWith("decaptcha ")){
						focusItem = input.substring(10);
					}
					System.out.println("Focus Item: "+focusItem);

					// check that inventory contains item
					if(p.getPlayerInventoryString(pid).contains(focusItem)){
						// add item to room
						rf.addToRoom(mid, tx, ty, rf.getItemID(focusItem));
						// remove item from inventory
						rf.removeFromInventory(pid, rf.getItemID(focusItem)); // works
						// Update GUI
						out.println("UPDATE INVENTORY "+ p.getPlayerInventoryString(pid));
						// Get Items
						getRoomItemsArray(mid,tx,ty);
					}
					else{
						out.println("GAME DISP " + focusItem + " is not accessable.");
					}
				}
				else if(input.startsWith("inspect ")){
					// Get Focus
					focusItem = input.substring(8);
					// Check that FocusItem exists
					if(arrayOfRoomItems.contains(focusItem) || p.getPlayerInventoryIDs(pid).contains(focusItem)){
						// Get description and Display it
						out.println("GAME DISP "+rf.getItemDescription(focusItem));
					}
					else{
						out.println("GAME DISP "+focusItem+" is not accessable.");
					}
				}
				else if(input.startsWith("actions")){
					// create string of available options
					// Query
					out.println("GAME Q | Strife Specibus | Sprite | Cancel");
					input = i.waitForInput().toLowerCase();
					if(input.startsWith("strife specibus")){

					}
					else if(input.startsWith("sprite ")){

					}
					else if(input.startsWith("Cancel ") || input.startsWith("c ")){
						out.println("GAME Q Leave | Use X | [C]aptcha/[D]ecaptcha X | Inspect X | Actions");
					}
				}
				else{
					out.println("GAME DISP Command not understood.");
				}
				try {
					TimeUnit.MILLISECONDS.sleep(100);
				} catch (InterruptedException e) {
					System.out.println("Sleep failed in Room");
					e.printStackTrace();
				}
			}
		}
			//roomLoop = false; // Kills loop for shutdown
			//System.out.println("Error in Accessing Room.");





	//// INITIATING ROOM
	private int getMapType(int MapID, int x, int y){
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ALT FROM ALLROOMTYPES WHERE MapID = "+MapID+" AND X = "+ x +" AND Y = "+y+";" );
			while ( rs.next() ) {
				ret = rs.getInt("ALT");
				System.out.println("MID:" + MapID + " x:"+x+" y:"+y+" Alt:"+ret + "GetMapType");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		// return int
		if(ret == -1){
			System.out.println("Get Map Type Returned -1");
		}
		return ret;
	}
	private void getLeaveArray(int MapID, int x, int y){
		Connection c = null;
		Statement stmt = null;
		leaveOptionsStr.clear(); // reset variable
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT LEAVETO FROM ALLMAPLEAVE WHERE MapID = "+MapID+" AND X = "+ x +" AND Y = "+y+";" );
			while ( rs.next() ) {
				String leaveStr = rs.getString("LEAVETO");
				//System.out.println("Adding "+ leaveStr + " to tleave");
				leaveOptionsStr.add(leaveStr); // add all returned leave strings to the array
				//System.out.println("Added "+ leaveStr + " to tleave");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.out.println("GetLeaveArray Broke");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	private void getRoomItemsArray(int mapID, int x, int y){
		// Reset Variables
		arrayOfRoomItems.clear();
		roomItemsStr = "";

		// Get Room Items Array (For Processing Removal and Addition)
		arrayOfRoomItems = rf.getRoomItemArray(mapID, x, y);

		// Process ItemsArray to get Room Items String (To Display)
		for(int i = 0; i < arrayOfRoomItems.size();i++){
			String thisWord = arrayOfRoomItems.get(i);
			System.out.println("ThisWord: " + thisWord);
			// STRING OF STRINGS	
			if(roomItemsStr != ""){
				roomItemsStr = roomItemsStr + " | " + thisWord.substring(0, 1)+thisWord.substring(1);
			}
			else{
				roomItemsStr = thisWord.substring(0, 1)+thisWord.substring(1);
			}
		}
	}




	//// UTILITIES

	public String arrayToString(ArrayList<String> list){    
		StringBuilder builder = new StringBuilder(list.size());
		for(String str: list)
		{
			// Capitalize
			str = str.substring(0, 1).toUpperCase() + str.substring(1);
			builder.append(str + " | ");
		}

		if(builder.equals("")){
			String empty = "There is nothing here.";
			return empty;
		}
		else{
			return builder.toString();
		}
	}

	/*private String waitForInput(){
		String str = "";
		Boolean lookingForInput = true;
		while(lookingForInput){
			if(input.equals("")){
				try {
					//System.out.println("Sleep 10ms");
					TimeUnit.MILLISECONDS.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else{
				System.out.println("RH Input: " + input);
				str = input;
				lookingForInput = false;
				input = "";
			}
		}
		System.out.println("WaitForInput rh Returning: "+ str);
		return str;		
	}*/

	//public void acceptInput(String gPut) {
		//input = gPut;
	//}
	
	public void mainMenu(){
		out.println("GAME Q Leave | Use X | [C]aptcha/[D]ecaptcha X | Inspect X | Actions");
	}
	
	public void closeAllRoomLoops(){
		roomLoop = false;
		
	}
}
