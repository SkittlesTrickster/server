package gamePlay;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.TimeUnit;

import server.InputHandler;
import databaseFunctions.PlayerHandling;
import databaseFunctions.RoomFunctions;

public class Use {
	InputHandler i;
	RoomFunctions rf;
	PlayerHandling p;
	// Read Write
	//BufferedReader in;
	PrintWriter out;

	// initiate timer
	Timer timer = new Timer();
	ScheduleTask sched = new ScheduleTask();

	// Variables
	String useWith = "none";
	int maxTime = 0;
	int pid = -1;

	public Use(PrintWriter o, RoomFunctions trf, PlayerHandling tp, InputHandler ip){
		rf = trf;
		p = tp;
		out = o;
		i = ip;
	}

	public void storeUsePID(int uPID){
		// store pid
		pid = uPID;
	}

	public void useThing(String itemName, int mid, int tx, int ty, ArrayList<String> roomInventory){
		// Get UseType
		String useType = rf.getUseTypeFromName(itemName);

		// TODEM LATHE
		if(useType.equals("totem lathe")){
			// Ask for input card
			out.println("GAME Q What would you like to use with the Todem lathe?");
			// Recieve
			useWith = "none";
			useWith = i.waitForInput().toLowerCase();
			// Check if useWith is in the room
			if(roomInventory.contains(useWith)){
				// check what useWith is
				if(useWith.equals("pre-punched card")){
					// remove pre-punched card
					rf.removeFromRoom(mid, tx, ty, rf.getItemID("pre-punched card"));
					// create todem
					int totemID = rf.getItemID("totem");
					rf.addToRoom(mid, tx, ty, totemID);
					out.println("GAME DISP The machine whirrs to life creating a totem.");
					sleep(10);
				}
				else{
					out.println("GAME DISP "+useWith+" is not compatible with the Todem Lathe.");
				}
			}
			else{
				out.println("GAME DISP "+useWith+" is not accessable.");
			}
		}
		// ALCHEMITER
		else if(useType.equals("alchemiter")){
			// Ask what to use	
			out.println("GAME Q What would you like to use with the Alchemiter?");
			// Recieve
			String useWith = "none";
			useWith = i.waitForInput().toLowerCase();
			// Check if in room
			if(roomInventory.contains(useWith)){
				// Check if Valid Item
				if(useWith.equals("totem")){
					// remove totem
					rf.removeFromRoom(mid, tx, ty, rf.getItemID("totem"));
					// create cruxite artifact
					rf.addToRoom(mid, tx, ty, rf.getItemID("cruxite artifact"));
					out.println("GAME DISP The machine whirrs to life creating a cruxite artifact.");
					sleep(10);
				}
				else{
					out.println("GAME DISP "+useWith+" is not compatible with the Alchemiter.");
				}
			}
			else{
				out.println("GAME DISP "+useWith+" is not accessable.");
			}
		}
		// CRUXTRUDER
		else if(useType.equals("cruxtruder")){
			if(rf.checkTFBoolean("CruxtruderOpened", pid)){ // True
				// add Cruxite to room
				rf.addToRoom(mid, tx, ty, rf.getItemID("cruxite"));
				out.println("GAME DISP The machine spits out a piece of cruxite.");
				sleep(10);
			}
			else{
				// Open
				rf.setTFBoolean(pid, "CruxtruderOpened", "t");
				// Start Timer
				maxTime = rf.getTime(pid);
				timer.scheduleAtFixedRate(sched, 10, 1000); // start timer
				rf.setTFBoolean(pid,"RunTimer", "t"); // Set Variable to run again if reloaded
				// Create Sprite
				// Create Cruxite
				rf.addToRoom(mid, tx, ty, rf.getItemID("cruxite"));
				out.println("GAME DISP The machine spits out a piece of cruxite and an oddly spazzing sphere.");
				sleep(10);
			}
		}
		// PRE PUNCHED CARD
		//else if(useType.equals("pre-punched card")){}
		// COMPUTER
		else if(useType.equals("computer")){
			Boolean computerIsOn = true;
			while(computerIsOn){
				out.println("GAME Q Sburb_Client.exe   Sburb_Server.exe   Goggle.com   FireFish.exe   |Close|");
				String input = "none";
				input = i.waitForInput().toLowerCase();
				System.out.println(input);
				//computerIsOn = false; // Kills loop for shutdown
				// CLIENT CONNECTING TO SERVER
				if(input.equals("sburb_client.exe")){
					setupServer(pid, mid, tx, ty, roomInventory);
				}
				// SERVER Controls
				else if(input.equals("sburb_server.exe")){
					serverControls(pid);
				}
				else if(input.equals("close")){
					out.println("GAME DISP You turned the computer off.");
					computerIsOn = false;
				}
				else{
					out.println("GAME DISP "+ input +" is not responding.");
				}
				sleep(100); // Sleep between actions and next menu
			}
		}
		// SPRITE
		else if(useType.equals("sprite")){

		}
		// BUFF
		else if(useType.equals("buff")){

		}
		// CONTAINER
		else if(useType.equals("container")){

		}
		// CREATE
		else if(useType.equals("create")){

		}
		// WEARABLE
		//else if(useType.equals("wearable")){}
		else if(useType.equals("entermedium")){
			// Change this to get the Artifact's name from ALLPLAYERS
			out.println("GAME DISP You break the Cruxite Artifact." + '\n' + "The world spins around you for a moment. Upon recovering you notice that the quality of the light coming in through the windows has changed.");
			// Stop Timer
			timer.cancel();
			// EnteredMedium is true
			rf.setTFBoolean(mid, "EnteredMedium", "t");
		}
		// SBURBCD
		else if(useType.equals("sburbcd")){
			setupServer(pid, mid, tx, ty, roomInventory);
		}
		else{
			out.println("GAME DISP UseType: "+ useType +" not understood.");
		}
	}

	private void serverControls(int pid) {
		// Get pid of Client
		int clientID = p.getClient(pid);
		System.out.println("ClientID:" + clientID);
		// Get House ID of Client
		int cHouseID = rf.getMID("house", clientID, p.getSID(clientID));

		// Check that a connection exists
		if(clientID != -1){
			Boolean servingClient = true;
			out.println("GAME DISP Connecting to Client ...");
			sleep(500);
			while(servingClient){
				Boolean modifyingRoom = false;
				ArrayList<String> tempItemsArray;
				String room = "none";
				// Query rooms in client's house
				out.println("GAME DISP Choose a room:");
				String roomList = "| Bedroom | Bathroom | Upstairs Hallway | Livingroom | Kitchen | Downstairs Hallway |"
						+ " Parents Room | Garage | Attic | Roof | Front Yard | Back Yard | [x]Cancel";
				out.println("GAME Q " + roomList);

				room = i.waitForInput().toLowerCase();

				if (room.equals("cancel")){
					servingClient = false;
				}
				else{
					if(roomList.toLowerCase().contains(room)){
						modifyingRoom = true;
					}
					else{
						out.println("GAME DISP " + room + " does not exist.");
					}
				}
				while(modifyingRoom){
					int currentX = returnX(room);

					// Display items in that room
					tempItemsArray = rf.getRoomItemArray(cHouseID, currentX, 0);
					String itemsString = "none";
					for(int i = 0; i<tempItemsArray.size();i++){
						if(i == 0){
							itemsString = tempItemsArray.get(i);
						}
						else{
							itemsString = itemsString + " | " + tempItemsArray.get(i);
						}
					}
					out.println("GAME DISP " + itemsString);
					// Display server actions
					out.println("GAME Q | [M]ove x | [B]uild | [P]hernalia Registry | [C]hange Room | [X]Close "); // Can move item into sprite
					String choice = "none";
					choice = i.waitForInput().toLowerCase();

					if(choice.contains("m ")||choice.contains("move ")){
						String focusItem = "none";
						if(choice.contains("m ")){
							focusItem = choice.substring(2);
						}
						else if(choice.contains("move ")){
							focusItem = choice.substring(5);
						}
						// Get New Location
						out.println("GAME DISP Where do you want to move " + focusItem + "?"); 
						sleep(400);
						out.println("GAME Q | Bedroom | Bathroom | Upstairs Hallway | Livingroom | Kitchen | Downstairs Hallway |"
								+ " Parents Room | Garage | Attic | Roof | Front Yard | Back Yard |");
						choice = "none";
						choice = i.waitForInput().toLowerCase();	
						int newX = returnX(choice);
						//Move item
						// remove
						rf.removeFromRoom(cHouseID, currentX, 0, rf.getItemID(focusItem));
						// add
						rf.addToRoom(cHouseID, newX, 0, rf.getItemID(focusItem));
						out.println("GAME DISP "+ focusItem + " successfully moved to "+ choice+"."); 
					}
					else if(choice.equals("b") || choice.equals("build")){
						// Display grist
						// Build functions
					}
					else if(choice.equals("p")||choice.equals("phernalia registry")){
						Boolean card = rf.checkTFBoolean("CardSet", clientID);
						Boolean crux = rf.checkTFBoolean("CruxSet", clientID);
						Boolean alch = rf.checkTFBoolean("AlchSet", clientID);
						Boolean lathe = rf.checkTFBoolean("LatheSet", clientID);
						System.out.println("card: " + card);
						System.out.println("crux: " + crux);
						System.out.println("alch: " + alch);
						System.out.println("lathe: " + lathe);
						// Show list of Buildable items
						out.println("GAME DISP Phernalia Registry:");
						if(!card){
							out.println("GAME DISP [P]re-Punched Card");
						}
						if(!crux){
							out.println("GAME DISP [C]ruxtruder");
						}
						if(!alch){
							out.println("GAME DISP [A]lchimeter");
						}
						if(!lathe){
							out.println("GAME DISP [T]otem Lathe");
						}
						if(card && crux && alch && lathe){
							out.println("GAME DISP No availible phernalia items.");
						}
						else{
							// Ask if building or cancel
							out.println("GAME DISP ------------------------------");
							out.println("GAME Q | [S]et x | [x]Cancel |");
							// Get input
							String input = "none";
							input = i.waitForInput().toLowerCase();
							if(input.contains("s ")||input.contains("set ")){
								String focusItem = "none";
								if(input.contains("s ")){
									focusItem = input.substring(2);
								}
								else if(input.contains("set ")){
									focusItem = input.substring(4);
								}

								if(focusItem.equals("pre-punched card")||focusItem.equals("p")){
									if(!card){
										rf.addToRoom(cHouseID, currentX, 0, rf.getItemID("pre-punched card"));
										rf.setTFBoolean(clientID, "CardSet", "t");
										card = true;
										out.println("GAME DISP Pre-Punched Card successfully set.");
									}
									else{
										out.println("GAME DISP Pre-Punched Card already used.");
									}
								}
								else if(focusItem.equals("alchemiter")||focusItem.equals("a")){
									if(!alch){
										rf.addToRoom(cHouseID, currentX, 0, rf.getItemID("alchemiter"));
										rf.setTFBoolean(clientID, "AlchSet", "t");
										alch = true;
										out.println("GAME DISP Alchemiter successfully set.");
									}
									else{
										out.println("GAME DISP Alchemiter already used.");
									}
								}
								else if(focusItem.equals("cruxtruder")||focusItem.equals("c")){
									if(!crux){
										rf.addToRoom(cHouseID, currentX, 0, rf.getItemID("cruxtruder"));
										rf.setTFBoolean(clientID, "CruxSet", "t");
										crux = true;
										out.println("GAME DISP Cruxtruder successfully set.");
									}
									else{
										out.println("GAME DISP Cruxtruder already used.");
									}
								}
								else if(focusItem.equals("totem lathe")||focusItem.equals("t")){
									if(!lathe){
										rf.addToRoom(cHouseID, currentX, 0, rf.getItemID("totem lathe"));
										rf.setTFBoolean(clientID, "LatheSet", "t");
										lathe = true;
										out.println("GAME DISP Totem Lathe successfully set.");
									}
									else{
										out.println("GAME DISP Totem Lathe already used.");
									}
								}
								else{
									out.println("GAME DISP "+focusItem+" is not availible.");
								}

							}
							else if(input.equals("x")||input.equals("cancel")){

							}
							else{
								out.println("GAME DISP Command not understood.");
							}
						}
					}
					else if(choice.equals("c")||choice.equals("change room")){
						modifyingRoom = false;
					}
					else if(choice.equals("x")||choice.equals("close")){
						modifyingRoom = false;
						servingClient = false;
					}
					else{
						out.println("GAME DISP " + choice + " not understood.");
					}
					sleep(500);
				}
				//modifyingRoom = false; // Kills loop for shutdown
			}
			//servingClient = false; // Kills loop for shutdown
		}
		else{
			out.println("GAME DISP You do not have a Client player.");
		}
	}

	private int returnX(String room) {
		int ret = 0; // bedroom

		if(room.equals("bedroom")){
			ret = 0;
		}
		else if(room.equals("bathroom")){
			ret = 1;
		}
		else if(room.equals("upstairs hallway")){
			ret = 2;
		}
		else if(room.equals("downstairs hallway")){
			ret = 3;
		}
		else if(room.equals("livingroom")){
			ret = 4;
		}
		else if(room.equals("kitchen")){
			ret = 5;
		}
		else if(room.equals("parents room")){
			ret = 6;
		}
		else if(room.equals("attic")){
			ret = 7;
		}
		else if(room.equals("roof")){
			ret = 8;
		}
		else if(room.equals("front yard")){
			ret = 9;
		}
		else if(room.equals("back yard")){
			ret = 10;
		}
		else if(room.equals("garage")){
			ret = 11;
		}
		return ret;
	}

	private void setupServer(int pid, int mid, int tx, int ty, ArrayList<String> roomInventory){
		if(roomInventory.contains("sburbcd")||p.getPlayerInventoryString(pid).contains("sburbcd"))
			if(roomInventory.contains("computer")){
				// Connect to Server Player
				out.println("GAME DISP Connecting to skaianet ... Please Wait ...");
				sleep(1000); // 1 second
				//// Ask for player handle
				out.println("GAME Q Please enter your server player's handle:");
				String serverHandle = "none";
				serverHandle = i.waitForInput();
				int serverPID = p.getPID(serverHandle);
				int playerSession = p.getSID(pid);
				int serverSession = p.getSID(serverPID);
				int retServID = p.getServer(pid);
				int retClientID = p.getClient(serverPID);
				//// Check that client does not already have a server
				if(retServID == -1){
					//// Check that server does not already have a client
					if(retClientID == -1){
						//// Check that player is in same session
						if(serverPID != -1){
							if(playerSession != -1){
								if(playerSession == serverSession) {
									// Assign Server Player
									p.setServer(pid, serverPID);
									out.println("GAME DISP Successfully connected to player " + serverHandle);
									// Delete CD
									rf.removeFromRoom(mid, tx, ty, rf.getItemID("sburbcd"));
								}
								else{
									out.println("GAME DISP Could not connect.");
									sleep(500);
									out.println("GAME DISP Player not found in Session.");
									sleep(500);
									// If assign fails cd pops back out
									out.println("GAME DISP The SburbCD is ejected from the cd drive.");
								}
							}
							if(playerSession == -1){
								out.println("GAME DISP Could not connect.");
								sleep(500);
								out.println("GAME DISP Error404 Session not found.");
							}
							if(serverSession == -1){
								out.println("GAME DISP Could not connect.");
								sleep(500);
								out.println("GAME DISP No session found containing player "+serverHandle+".");
							}
						}
						else{
							out.println("GAME DISP Could not connect.");
							sleep(500);
							out.println("GAME DISP Player "+serverHandle+" does not exist.");
						}
					}
					else{
						out.println("GAME DISP Player "+serverHandle+" already has a Client.");
					}
				}
				else{
					out.println("GAME DISP "+p.getHandle(retServID)+" is your Server.");
				}
			}
			else{
				out.println("GAME DISP A computer is not accessable. The SburbCD requires a computer to run.");
			}
		else{
			out.println("GAME DISP SburbCD is not accessable.");
		}
		sleep(500); // sleep before returning to previous screen
	}

	private void sleep(int miliseconds) {
		try {
			TimeUnit.MILLISECONDS.sleep(miliseconds);
		} catch (InterruptedException e) {
			System.out.println("Sleep failed in Use");
			e.printStackTrace();
		}
	}

	class ScheduleTask extends TimerTask {
		public void run() {
			maxTime--; // reduce maxTime every second
			if(pid > -1){
				rf.setTime(pid, maxTime); // update table
				out.println("UPDATE TIMER " + maxTime);// update gui
				System.out.println("PID:" +pid+" Timer:"+maxTime);
			}
			else{
				System.out.println("tpid is -1");
				timer.cancel();
			}
			if(maxTime == 0){
				// Kill Player
				out.println("GAME DISP - BOOM -");
				sleep(10);
				out.println("GAME DISP A meteor crashes into your house ending your game before it has even begun.");
				//out.println("GAMEOVER");
				// Stop Timer
				timer.cancel();
			}
		}
	}

	public void startTimer(int time) {
		if(time > 0){
			maxTime = time;
			timer.scheduleAtFixedRate(sched, 10, 1000); // start timer
		}
	}

	public void stopTimer(){
		timer.cancel();
	}
	public void stopUseLoops(){
		//computerIsOn = false;
		//modifyingRoom = false;
		//servingClient = false;
	}
}
