package databaseFunctions;

import generation.GenItems;
import generation.PlanetGen;
import generation.PlyrGen;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Random;

import maps.DreamingPlanet;
import maps.House;
import maps.Map;
//import java.util.Scanner;
import maps.Planet;
import maps.Skaia;

public final class MainMenuFunctions {
	Random rand = new Random();
	//Scanner scan = new Scanner(System.in);
	PlyrGen plyrGen = new PlyrGen();
	PlanetGen planetGen = new PlanetGen();
	static MapFunctions mf = new MapFunctions();
	public GenItems gi = new GenItems(mf);


	String dbPath = "TESTSKAIA.db";

	String retrievedPass;

	Boolean loginAccepted = false;

	public void createPlayer(String pName, String handle, String password, String gender){
		int playerID = -1;
		int planetID = -1;
		int houseID = -1;
		String planetName01 = "noName";
		String planetName02 = "noName";
		String consort = "skeleton";
		// PLANET COLORS // Defaults
		String color01 = "0xFFCC99";
		String color02 = "0xFF9966";
		String color03 = "0xCC9933";
		String color04 = "0x996600";
		String color05 = "0x663300";
		String colorWater = "0x99CCFF";

		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			System.out.print(pName +" | ");
			// ASPECT
			String hsclass = plyrGen.genClass();
			String aspect = plyrGen.genAspect();
			System.out.print(hsclass +" of " + aspect+" | ");

			// PLANET NAME
			planetName01 = planetGen.genLand01(aspect);
			planetName02 = planetGen.genLand02(aspect);
			System.out.print("Land of " +planetName01+" and "+planetName02+" | ");

			// CONSORTS
			consort = planetGen.genConsort();
			System.out.print(consort +'\n');

			// DREAMER
			String dreamer = plyrGen.genDreamer();

			/*// PLANET COLORS // Generate them here
			color01 = "0xFFCC99";
			color02 = "0xFF9966";
			color03 = "0xCC9933";
			color04 = "0x996600";
			color05 = "0x663300";
			colorWater = "0x99CCFF";*/

			//// PLAYER
			// Create Player
			stmt = c.createStatement();
			String sql = "INSERT INTO ALLPLAYERS (PlayerID,MapID,SessionID,Handle,Name,Password,Gender,Rung,Boondollars,Class,Aspect,Location,HouseID,x,y,Specibus01,Specibus02,Specibus03,Server,Client,Dreamer,RungName,HP) " +
					"VALUES (null, -1 ,-1,'"+handle+"','"+pName+"','"+password+"','"+gender+"', 0, 0, '"+hsclass+"','"+aspect+"','house', -1 , 0, 0, 'unset','unset','unset',-1,-1,'"+dreamer+"', 'Green CandyHorn',100);"; 
			stmt.executeUpdate(sql);
			// Get PlayerID
			playerID = stmt.getGeneratedKeys().getInt(1);
			System.out.println("PlayerID: " + playerID);

			// Player Triggers
			// Generate timer
			int maxTime = rand.nextInt(600)+300; // 300seconds = 5minutes min - 900seconds = 15minutes max
			// Create triggers
			stmt = c.createStatement();
			sql = "INSERT INTO TRIGGERS (TID,PlayerID,MeteorTimer,EnteredMedium,SpriteReleased,SpriteLocation,SpriteName,ProtoTypeCount,IsAwake,CardSet,AlchSet,CruxSet,LatheSet,CruxtruderOpened,RunTimer,AlchimeterUpgradeLv) " +
					"VALUES (null,"+playerID+","+maxTime+",'F','F','unset','kernelsprite',0,'F','F','F','F','F','F','F',0);"; 
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in create player part 1");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		// CREATE HOUSE
		houseID = createMapAndReturnMid(-1, playerID + "house", 12, 1 ); // House Map Dimensions
		System.out.println("HouseID created");

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);


			// INSERT PLANET into ALLMAPIDS
			stmt = c.createStatement();
			String sql = "INSERT INTO ALLMAPIDS (MapID,SessionID,Location,Width,Height)" +
					"VALUES (null,-1,'"+ playerID +"planet', 20 ,20);"; // Dimensions
			System.out.println("Successfully Added MapID"+planetID+" to ALLMAPIDS");
			stmt.executeUpdate(sql);
			// Get PlanetID
			planetID = stmt.getGeneratedKeys().getInt(1);
			System.out.println("MapID: " + planetID);
			// INSERT PLANET into ALLPLANETS
			stmt = c.createStatement();
			sql = "INSERT INTO ALLPLANETS (PKID, MapID, SessionID, PlanetName01,PlanetName02,Denizen,Consorts,Gate01,Gate02,Gate03,Gate04,Gate05,Gate06,Gate07,Color01,Color02,Color03,Color04,Color05,WaterColor,HouseX,HouseY) " +
					"VALUES (null, "+ planetID +", -1 ,'"+planetName01+"','"+planetName02+"','unset','"+ consort +"','unset','unset','unset','unset','unset','unset','unset', '"+ color01 +"','"+ color02 +"','"+ color03 +"','"+ color04 +"','"+ color05 +"','"+ colorWater +"',0,0);";
			stmt.executeUpdate(sql);

			// Update player SID and HID
			// HouseID

			stmt = c.createStatement();
			sql = "UPDATE ALLPLAYERS set MapID = "+planetID+" where PlayerID="+playerID+";";
			System.out.println("Updated player " + playerID + " to PlanetMID" + planetID);
			stmt.executeUpdate(sql);
			// PlanetID (MapID)
			stmt = c.createStatement();
			sql = "UPDATE ALLPLAYERS set HouseID = "+houseID+" where PlayerID="+playerID+";";
			System.out.println("Updated player " + playerID + " to HouseID" + houseID);
			stmt.executeUpdate(sql);


			stmt.close();
			c.commit();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in create player part 2");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		// ----------------------------------------------------------------------------------------------------
		//// GENERATE HOUSE MAP
		House h = new House(gi);
		writeTLIArrays(houseID, h.returnMap()); // store Types, Leave, and Items

		//// GENERATE PLANET MAP
		Planet p = new Planet(gi);
		changeHousePos(planetID, h.getHX(), h.getHY());
		writeTLIArrays(planetID, p.returnMap());
		
		// TODO increase skaia
		// TODO increase dreaming moon towers
		// TODO assign moon tower
		// ----------------------------------------------------------------------------------------------------
		System.out.println("Player created successfully");
	}

	private int createMapAndReturnMid(int sid, String locName, int w, int h) {
		System.out.println("Creating map: "+ locName);
		Connection c = null;
		Statement stmt = null;
		int MapID = -1;

		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "INSERT INTO ALLMAPIDS (MapID,SessionID,Location,Width,Height)" +
					"VALUES (null,"+sid+",'"+ locName +"', "+w+" ,"+h+");";
			stmt.executeUpdate(sql);

			MapID = stmt.getGeneratedKeys().getInt(1);
			System.out.println("Successfully Added "+ locName +" mid:"+MapID+" to ALLMAPIDS");

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Create Map and return PID failed");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		} 
		if(MapID == -1){
			System.out.println("MID returned -1 in createMapAndReturnMid");
		}
		return MapID;
	}

	private void writeTLIArrays(int MapID, Map m) { //TODO Take in a Map object instead
		System.out.println("Adding mapID "+MapID + " to Tables");
		Map map = m;
		// Assign generated arrays
		int[][] tType = m.getRoomTypeArray();
		ArrayList<ArrayList<ArrayList<String>>> tLeave = m.getLeaveArray();
		ArrayList<ArrayList<ArrayList<Integer>>> tItemIDs = m.getItemIDsArray();
		int xLim = map.getxLimit();
		int yLim = map.getyLimit();
		System.out.println("****** Writing Map xMax: "+xLim + " yMax: "+yLim+ " ******");
		Connection c = null;
		Statement stmt = null;

		try{
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			stmt = c.createStatement();

			// Add generated arrays to tables
			for(int y=0; y<yLim;){
				for(int x=0; x<xLim;){
					// ------------------------------------------------------
					// Room Type
					System.out.println("Writing RoomType("+x+","+y+") "+ tType[x][y]);
					String sql = "INSERT INTO ALLROOMTYPES (MapID,X,Y,ALT) " +
							"VALUES ("+MapID+","+ x +","+y+","+tType[x][y]+");";

					System.out.println("MID"+MapID+" X:"+ x +" Y:"+y+" Type:"+tType[x][y]);
					stmt.executeUpdate(sql);
					// ------------------------------------------------------
					// Leave
					System.out.print("Writing Leave("+ x +","+y+") ");
					for(int num=0; num < tLeave.get(y).get(x).size();){ // for as many times as there are entries
						sql = "INSERT INTO ALLMAPLEAVE (MapID,X,Y,LEAVETO) " +
								"VALUES ("+MapID+","+ x +","+y+",'"+tLeave.get(y).get(x).get(num)+"');";
						System.out.print(tLeave.get(y).get(x).get(num) +", ");
						stmt.executeUpdate(sql);
						num++;
					}
					System.out.print('\n');
					// ------------------------------------------------------
					// Items
					System.out.print("Writing ItemIDs("+ x +","+y+") ");
					for(int num=0; num < tItemIDs.get(y).get(x).size();){ // for as many times as there are entries
						sql = "INSERT INTO ALLMAPITEMS (MapID,X,Y,ItemID,QTY) " +
								"VALUES ("+MapID+","+ x +","+y+",'"+tItemIDs.get(y).get(x).get(num)+"',1);";
						System.out.print(tItemIDs.get(y).get(x).get(num) +", ");
						stmt.executeUpdate(sql);
						num++;
					}
					System.out.print('\n');
					// --------------------------------------------------------------------------------------------
					x++;
				}
				y++;
			}
			stmt.close();
			c.commit();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in StoreTLIArrays");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		/*// test
		//Connection c = null;
		//Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			int count = 0;
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ALT FROM ALLROOMTYPES WHERE MapID = "+MapID+";" );
			while ( rs.next() ) {
				System.out.println("Alt"+ count +": "+ rs.getInt("ALT"));
				count++;
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}*/

	}

	public void createSession(String sessName, String password){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Add Name and password to array
			stmt = c.createStatement();
			String sql = "INSERT INTO ALLSESSIONS (SessionID,SessName,SessPassword) " +
					"VALUES (null,'"+sessName+"','"+password+"');";
			stmt.executeUpdate(sql);
			
			int sid = stmt.getGeneratedKeys().getInt(1);

			// TODO Create All maps
			Skaia skaia = new Skaia(gi);
			DreamingPlanet prospit = new DreamingPlanet(gi);
			DreamingPlanet derse = new DreamingPlanet(gi);
			
			// TODO Moons
			// TODO Connections between maps
			// Initial Skaia
			int skaiaID = createMapAndReturnMid(sid, "skaia", 10, 10 );
			writeTLIArrays(skaiaID, skaia.returnMap());
			// Prospit Planet
			int prospitID = createMapAndReturnMid(sid, "prospit", 10, 10 );
			writeTLIArrays(prospitID, prospit.returnMap("prospit"));
			// Derse Planet
			int derseID = createMapAndReturnMid(sid, "derse", 10, 10 );
			writeTLIArrays(derseID, derse.returnMap("derse"));

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Session created successfully");

	}


	public boolean handleExists(String handleName){
		String handleList = selectWhereString("Handle","ALLPLAYERS","Handle", handleName);
		if(handleList.equals("null")){
			return false;
		}
		else{
			return true;
		}
	}

	public boolean sessionExists(String sessName){
		String sessList = selectWhereString("SessName", "ALLSESSIONS", "SessName", sessName);
		if(sessList.equals("null")){
			return false;
		}
		else{
			return true;
		}
	}

	public boolean verifyLogin(String handle, String password){
		Boolean b = false;
		Connection c = null;
		Statement stmt = null;
		//System.out.println("Verify Login Start");
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:"+dbPath);
			c.setAutoCommit(false);

			// Check Login Credentials
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Password FROM ALLPLAYERS WHERE Handle = '"+handle+"';" );

			while ( rs.next() ) {
				retrievedPass = rs.getString("Password");
				System.out.println("Retrieved: " + retrievedPass);
			}
			if(!retrievedPass.equals(null)){
				if(retrievedPass.equals(password)){
					b = true;
				}
				else{
					b = false;
				}
			}
			else{
				b = false;
			}
			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			b = false;
		}
		return b;
	}

	public int getPid(String handle, String password){
		int tempPid = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			// Get PlayerID
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT PlayerID FROM ALLPLAYERS WHERE Handle = '"+handle+"' AND Password = '"+password+"';" );
			while ( rs.next() ) {
				tempPid = rs.getInt("PlayerID");
			}

			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			//System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		}
		if(tempPid == -1){
			System.out.println("GetPid returned -1.");
		}
		System.out.println("getPid:"+ tempPid);
		return tempPid;
	}

	public int getSid(int pid){
		System.out.println("getSid: pid: " + pid);
		int tempSid = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			// Get SessionID
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT SessionID FROM ALLPLAYERS WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				tempSid = rs.getInt("SessionID");
				System.out.println("Returned SID#: " + tempSid);
			}
			/*if(tempSid == -1){
				stmt = c.createStatement();
				rs = stmt.executeQuery( "SELECT HouseID FROM ALLPLAYERS WHERE PlayerID = '"+pid+"';" );
				while ( rs.next() ) {
					tempSid = rs.getInt("HouseID");
					System.out.println("Returned HID#: " + tempSid);
				}
			}*/

			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			//System.err.println( e.getClass().getName() + ": " + e.getMessage() );
		}
		if(tempSid == -1){
			System.out.println("ERROR: GetSid returned -1.");
		}
		return tempSid;
	}

	public int verifyJoin(String name, String password) {
		int sessionID = -1;

		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT SessionID FROM ALLSESSIONS WHERE SessName = '"+name+"' AND SessPassword = '"+password+"';" );
			while ( rs.next() ) {
				sessionID = rs.getInt("SessionID");
				System.out.println("Joining sessionID: "+ sessionID);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
		}
		if(sessionID == 0 || sessionID == -1){
			System.out.println("VerifyJoin Session Id Failed:"+sessionID);
		}
		return sessionID;
	}

	public void linkSessionIDs(int pid,int sid) {

		Connection c = null;
		Statement stmt = null;
		try {
			int tmid = -1;
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			ResultSet rs;

			// Set Player Sid
			stmt = c.createStatement();
			String sql = "UPDATE ALLPLAYERS set SessionID = "+sid+" where PlayerID="+pid+";";
			stmt.executeUpdate(sql);

			// Get HID
			int hid = -1;
			System.out.println("GetHID looking for HouseID: "+hid+" for PlayerID: " + pid);
			rs = stmt.executeQuery( "SELECT HouseID FROM ALLPLAYERS WHERE PlayerID = "+ pid +";" );
			while ( rs.next() ) {
				hid = rs.getInt("HouseID");
			}
			if(hid == -1){
				System.out.println("Hid = -1 in LinkSessionIds");
				System.exit(0);
			}

			// Set House Sid
			stmt = c.createStatement();
			sql = "UPDATE ALLMAPIDS set SessionID = "+sid+" where MapID="+hid+";";
			System.out.println("SessionID set to " + sid + " for HouseID " + hid);
			stmt.executeUpdate(sql);

			// Get Planet MapID from Player
			stmt = c.createStatement();
			rs = stmt.executeQuery("SELECT MapID FROM ALLPLAYERS WHERE PlayerID = "+pid+";");
			while ( rs.next() ) {
				tmid = rs.getInt("MapID");
			}
			rs.close();
			if(tmid == -1){
				System.out.println("MapID returned -1 in setSessionID");
			}

			// Set Planet Sid
			stmt = c.createStatement();
			sql = "UPDATE ALLPLANETS set SessionID = "+sid+" where MapID="+tmid+";";
			stmt.executeUpdate(sql);

			// Set AllMapID Sid
			stmt = c.createStatement();
			sql = "UPDATE ALLMAPIDS set SessionID = "+sid+" where MapID="+tmid+";";
			stmt.executeUpdate(sql);

			System.out.println("Successfully Updated SessionID:" + sid + " for MapID: "+tmid + " and PlayerID: " + pid);

			c.commit();
			stmt.close();
			c.close();
		}
		catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.out.println("Could not update player" +pid+" to session" + sid + " in setSessionID");
		}
	}

	public String getPlayerLocation(int pid){
		String ret = null;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Location FROM ALLPLAYERS WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				ret = rs.getString("Location");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(ret.equals(null)){
			System.out.println("Player "+pid+"'s Location is null");
		}
		return ret;
	}


	//-------------------------------------------------------------------------------------------------------
	public String selectWhereString(String getThis, String table, String columnHead, String columnEntry){
		String ret = null;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			//System.out.println("Opened database successfully");

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT "+getThis+" FROM "+table+" WHERE "+columnHead+" = '"+columnEntry+"';" );
			while ( rs.next() ) {
				ret = rs.getString(getThis);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(ret == null){
			return "null";
		}
		else{
			return ret;
		}
	}	

	public void changeHousePos(int mid, int hx, int hy){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE ALLPLANETS set HouseX = '"+hx+"' where MapID = "+mid+";";
			stmt.executeUpdate(sql);

			stmt = c.createStatement();
			sql = "UPDATE ALLPLANETS set HouseY = '"+hy+"' where MapID = "+mid+";";
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Change House Pos");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
}
