package databaseFunctions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public final class RoomFunctions {
	// GetInventoryString String Array
	//public ArrayList<String> roomItemsArray = new ArrayList<String>();
	//public String itemsStr = null;
	
	public String sendMapTypes(int mid){
		// get roomTypes based on mid ordered by x then y
		Connection c = null;
		Statement stmt = null;
		String mapTypes = "";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet width from mid
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ALT FROM ALLROOMTYPES WHERE MapID = '"+mid+"' ORDER BY Y, X;" );
			while ( rs.next() ) {
				if(mapTypes.equals("")){
					mapTypes = "" + rs.getInt("ALT");
				}
				else{
				mapTypes = mapTypes + " " + rs.getInt("ALT");
				}
			}
			rs.close();
			stmt.close();
			c.close();
			if(mapTypes.equals("")){
				System.out.println("No MapTypes returned in sendMapTypes");
			}

		} catch ( Exception e ) {
			System.out.println("Error in Send Map Types");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			//System.exit(0);
		}
		return mapTypes;
		// send maptypes
		// send xy in roomHandler
	}
	
	public ArrayList<String> getRoomItemArray(int mapID, int x, int y){
		Connection c = null;
		Statement stmt = null;
		ArrayList<Integer> roomItemIDs = new ArrayList<Integer>();
		ArrayList<String> roomItemStrings = new ArrayList<String>();
		
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			// GET ITEMIDS
			stmt = c.createStatement();
			System.out.println("Get ItemArray: MID = "+mapID+" X = "+ x +" Y = "+y);
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM ALLMAPITEMS WHERE MapID = "+mapID+" AND X = "+ x +" AND Y = "+y+";" );
			while ( rs.next() ) {
				int id = rs.getInt("ItemID");
				roomItemIDs.add(id); // add all returned strings to the array
				System.out.print(id + ", ");
			}
			System.out.print('\n');

			// GET NAME ARRAY
			System.out.println("Get ItemName:");
			for(int i=0; i < roomItemIDs.size();i++){
				stmt = c.createStatement();
				System.out.print("Get ItemName: IID = "+roomItemIDs.get(i));
				rs = stmt.executeQuery( "SELECT ItemName FROM GLOBALITEMS WHERE ItemID = '"+roomItemIDs.get(i)+"';" );
				while ( rs.next() ) {
					String nam = rs.getString("ItemName");
					System.out.print(" Name: = "+nam);
					// ARRAY OF STRINGS
					roomItemStrings.add(nam);
				}
				System.out.print('\n');
			}

			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.out.println("GetItemsArray Broke");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(roomItemIDs.isEmpty()){
			System.out.println("RoomItemIDs is empty.");
		}
		return roomItemStrings;

	}

	/*public void getRoomItems(ArrayList<Integer> roomIDlist){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			for(int i=0; i < roomIDlist.size();i++){
				// get name from id in GlobalItems
				stmt = c.createStatement();
				ResultSet rs = stmt.executeQuery( "SELECT ItemName FROM GLOBALITEMS WHERE ItemID = '"+roomIDlist.get(i)+"';" );
				while ( rs.next() ) {
					String nam = rs.getString("ItemID");
					// add name to an array of strings
					roomItemsArray.add(nam);
					// add name to whole list string	
					itemsStr = itemsStr + " | " + nam.substring(0, 1)+nam.substring(1);
				}
				rs.close();
			}
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in getRoomItems");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}*/

	public int getItemID(String focusItem){
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM GLOBALITEMS WHERE ItemName = '"+focusItem+"';" );
			while ( rs.next() ) {
				ret = rs.getInt("ItemID");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(ret < 0){
			System.out.println("Error in GetItemID when selecting: " + focusItem);
		}
		return ret;	
	}

	public String getItemDescription(String focusItem){
		String ret = null;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Description FROM GLOBALITEMS WHERE ItemName = '"+focusItem+"';" );
			while ( rs.next() ) {
				ret = rs.getString("Description");
			}
			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Exception in GetItemDescription when selecting: " + focusItem);
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(ret.equals(null)){
			System.out.println("Null in GetItemDescription when selecting: " + focusItem);
		}
		return ret;	
	}

	public void addToRoom(int mid, int X, int Y, int itemID){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "INSERT INTO ALLMAPITEMS (MapID,X,Y,ItemID,QTY) " +
					"VALUES ("+mid+", "+X+", "+Y+", "+itemID+", 1);"; 
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Add to Room");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
	public void removeFromRoom(int mid, int x, int y, int itemID){
		Connection c = null;
		Statement stmt = null;
		int itemKey = -1;
		//int ret = -1;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			// Get ItemKey
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemKey FROM ALLMAPITEMS WHERE ItemID = "+itemID+" AND X = "+x+" AND Y = "+y+" AND MapID = "+mid+" LIMIT 1;" );
			while ( rs.next() ) {
				itemKey = rs.getInt("ItemKey");
			}
			System.out.println("ItemKey: "+ itemKey + " ItemID: "+ itemID);

			stmt = c.createStatement();
			String sql = "DELETE from ALLMAPITEMS where ItemKey="+itemKey+";";
			stmt.executeUpdate(sql);
			System.out.println("Deleted "+itemKey+" from "+ mid +" " + x + " " + y);

			rs.close();
			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Remove from Room");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public void addToInventory(int pid, int itemID){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "INSERT INTO ALLINVENTORIES (ItemKey, PlayerID, ItemID) " +
					"VALUES (null, "+pid+", "+itemID+");"; 
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Add to Inventory");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public void removeFromInventory(int pid, int itemID){
		Connection c = null;
		Statement stmt = null;
		int itemKey = 0;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();

			// Get ItemKey
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemKey FROM ALLINVENTORIES WHERE ItemID = "+itemID+" AND PlayerId = "+pid+" LIMIT 1;" );
			while ( rs.next() ) {
				itemKey = rs.getInt("ItemKey");
			}
			System.out.println("ItemKey: "+ itemKey + " ItemID: "+ itemID);

			stmt = c.createStatement();
			String sql = "DELETE from ALLINVENTORIES where ItemKey="+itemKey+";";
			stmt.executeUpdate(sql);
			System.out.println("Deleted:"+itemKey+" for pid:"+ pid);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Remove from Inventory");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(itemKey == 0){
			System.out.println("Itemkey nonexistant in remove from Inventory");
		}
	}

	public String getPlanetLoc(int pid){
		Connection c = null;
		Statement stmt = null;
		String land01 = null;
		String land02 = null;
		int planetID= -1;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet MapID from player
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT MapID FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
			while ( rs.next() ) {
				planetID = rs.getInt("MapID");
			}
			if(planetID == -1){
				System.out.println("No planet ID returned");
			}
			// Get landname01 from planet
			stmt = c.createStatement();
			rs = stmt.executeQuery( "SELECT PlanetName01 FROM ALLPLANETS WHERE MapID = "+planetID+";" );
			while ( rs.next() ) {
				land01 = rs.getString("PlanetName01");
			}
			// Get landname02 from planet
			stmt = c.createStatement();
			rs = stmt.executeQuery( "SELECT PlanetName02 FROM ALLPLANETS WHERE MapID = "+planetID+";" );
			while ( rs.next() ) {
				land02 = rs.getString("PlanetName01");
			}
			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in Get planet Location");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return land01 + land02;	
	}

	public boolean checkIfRemovableFromRoom(String itemName){
		Connection c = null;
		Statement stmt = null;
		String yn = null;
		Boolean ret = false;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet MapID from player
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Pickup FROM GLOBALITEMS WHERE ItemName = '"+itemName+"';" );
			while ( rs.next() ) {
				yn = rs.getString("Pickup");
			}
			if(yn.toLowerCase().equals("y")){
				ret = true;
			}
			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in Get Pickup");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;
	}

	public int getMapWidth(int mid) {
		Connection c = null;
		Statement stmt = null;
		int ret = -1;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet width from mid
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Width FROM ALLMAPIDS WHERE MapID = '"+mid+"';" );
			while ( rs.next() ) {
				ret = rs.getInt("Width");
				System.out.println("Width: " + ret);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("-1 Width returned in GetMapWidth");
			}

		} catch ( Exception e ) {
			System.out.println("Error in Get Map Width");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;
	}

	public int getMapHeight(int mid) {
		Connection c = null;
		Statement stmt = null;
		int ret = -1;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet width from mid
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Height FROM ALLMAPIDS WHERE MapID = '"+mid+"';" );
			while ( rs.next() ) {
				ret = rs.getInt("Height");
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("-1 Height returned in GetMapHeight");
			}

		} catch ( Exception e ) {
			System.out.println("Error in Get Map Height.");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;
	}
	
	public String getUseTypeFromName(String itemName){
		int id = getItemID(itemName);
		Connection c = null;
		Statement stmt = null;
		String ret = "none";
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet width from mid
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT UseType FROM GLOBALITEMS WHERE ItemID = '"+id+"';" );
			while ( rs.next() ) {
				ret = rs.getString("UseType");
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret.equals("none")){
				System.out.println("No item returned for name:"+ itemName +" id:"+id+ " in Get item Use From Name");
			}

		} catch ( Exception e ) {
			System.out.println("Error in Get Item Use From Name.");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;
	}
	
	public Boolean checkTFBoolean(String triggerName, int pid){
		Connection c = null;
		Statement stmt = null;
		String ret = "none";
		Boolean tf = false;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			// Get planet width from mid
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT "+ triggerName +" FROM TRIGGERS WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				ret = rs.getString(triggerName);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret.equals("none")){
				System.out.println("No boolean returned in check TF Boolean");
			}
			if(ret.equals("t")){
				tf = true;
			}
			if(ret.equals("f")){
				tf = false;
			}

		} catch ( Exception e ) {
			System.out.println("Error in checkTFBoolean.");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return tf;
	}
	
	public void setTFBoolean(int pid, String triggerName, String tf){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE TRIGGERS set "+triggerName+" = '"+tf+"' where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);;

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in SetTFBoolean");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
	
	public int getMID(String location, int pid, int sid){
		//System.out.println("getmid sid:"+sid);
		int ret = -1;
		// Use SessionID and location name to assign mid
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs;
			// HASN'T JOINED A SESSION
			if(sid == -1 && location.contains("house")){
				System.out.println("SID = -1 and location = house");
				int hid = -1;
				// get hid
				System.out.println("GetMID(RoomH) looking for HouseID: "+hid+" for PlayerID: " + pid);
				rs = stmt.executeQuery( "SELECT HouseID FROM ALLPLAYERS WHERE PlayerID = "+ pid +";" );
				while ( rs.next() ) {
					hid = rs.getInt("HouseID");
				}
				if(hid == -1){
					System.out.println("Hid = -1 in getMID");
					System.exit(0);
				}
				// search for hid
				System.out.println("GetHID(RoomH) looking for HouseID for player: " + pid);
				rs = stmt.executeQuery( "SELECT HouseID FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
				while ( rs.next() ) {
					ret = rs.getInt("HouseID");
				}
			}
			// HAS JOINED A SESSION
			else{
				if(location.contains("house")){
					location = pid+"house";
				}
				System.out.println("GetMID looking for SessionID: "+sid+" and Location: " + location);
				rs = stmt.executeQuery( "SELECT MapID FROM ALLMAPIDS WHERE SessionID = "+sid+" AND Location = '"+location+"';" );
				while ( rs.next() ) {
					ret = rs.getInt("MapID");
				}
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(ret == -1){
			System.out.println("getMID returned -1");
			// SessionID is null
		}
		return ret;
	}

	public int getTime(int pid) {
		Connection c = null;
		Statement stmt = null;
		int ret = -1;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT MeteorTimer FROM TRIGGERS WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				ret = rs.getInt("MeteorTimer");
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("No Timer returned in GetTime");
			}
			else{
				System.out.println("Timer retrieved at: " + ret + " for player: " + pid);
			}
		} catch ( Exception e ) {
			System.out.println("Error in GetTime.");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;
		
	}
	public void setTime(int pid, int time) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE TRIGGERS set MeteorTimer = "+time+" where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);;

			stmt.close();
			c.commit();
			c.close();
			System.out.println("MeteorTimer set to: " + time + " for player: " + pid);
		} catch ( Exception e ) {
			System.out.println("Error in SetTime");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		
	}
	
	
}
