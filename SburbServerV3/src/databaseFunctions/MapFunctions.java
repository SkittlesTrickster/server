package databaseFunctions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public final class MapFunctions {
	
	public int getItemID(String focusItem){
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM GLOBALITEMS WHERE ItemName = '"+focusItem+"';" );
			while ( rs.next() ) {
				ret = rs.getInt("ItemID");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		if(ret < 0){
			System.out.println("Error in GetItemID when selecting: " + focusItem);
		}
		return ret;	
	}

	public ArrayList<Integer> getIDlist(String locType){
		// TODO add option to select ALL
		ArrayList<Integer> IDlist = new ArrayList<Integer>();
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM GLOBALITEMS WHERE LocType01 = '"+locType+"' OR LocType02 = '"+locType+"' OR LocType03 = '"+locType+"';" );
			while ( rs.next() ) {
				IDlist.add(rs.getInt("ItemID"));
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return IDlist;
	}


}
