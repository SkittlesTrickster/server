package databaseFunctions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public final class PlayerHandling {
	String databaseLoc = "jdbc:sqlite:TESTSKAIA.db";

	public int getx(int playerID){
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT X FROM ALLPLAYERS WHERE PlayerID = "+playerID+";" );
			while ( rs.next() ) {
				ret = rs.getInt("X");
				System.out.println("Player X: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}	
	public int gety(int playerID){
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Y FROM ALLPLAYERS WHERE PlayerID = "+playerID+";" );
			while ( rs.next() ) {
				ret = rs.getInt("Y");
				System.out.println("Player Y: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}

	public int getHP(int pid){
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT HP FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
			while ( rs.next() ) {
				ret = rs.getInt("HP");
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}

	public ArrayList<String> getPlayerInventoryIDs(int pid){
		Connection c = null;
		Statement stmt = null;
		//Boolean ret = false;
		ArrayList<String> pewp = new ArrayList<String>();
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			// Get ItemID where PlayerID
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM ALLINVENTORIES WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				pewp.add(rs.getString("ItemID"));
			}
			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in Get Player Inventory");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

		return pewp;
	}

	public String getPlayerInventoryString(int pid){
		Connection c = null;
		Statement stmt = null;
		//Boolean ret = false;
		ArrayList<String> IDarray = new ArrayList<String>();
		String invString = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			// Get ItemID where PlayerID
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM ALLINVENTORIES WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				IDarray.add(rs.getString("ItemID"));
			}

			for(int i = 0; i< IDarray.size();i++){
				stmt = c.createStatement();
				rs = stmt.executeQuery( "SELECT ItemName FROM GLOBALITEMS WHERE ItemID = '"+IDarray.get(i)+"';" );
				while ( rs.next() ) {
					if(i == 0){
						invString = rs.getString("ItemName");
					}
					else{
						invString = invString +" " + rs.getString("ItemName");	
					}
				}
			}

			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in Get Player Inventory");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return invString;
	}

	public int countInventory(int pid){
		Connection c = null;
		Statement stmt = null;
		int invCount = 0;

		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			// Get ItemID where PlayerID
			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT ItemID FROM ALLINVENTORIES WHERE PlayerID = '"+pid+"';" );
			while ( rs.next() ) {
				invCount++;
			}
			rs.close();
			stmt.close();
			c.close();

		} catch ( Exception e ) {
			System.out.println("Error in Count Inventory");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return invCount;
	}

	public void changeHP(int pid, int hp){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE ALLPLAYERS set HP = '"+hp+"' where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);;

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Change HP");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public void changeX(int x, int pid){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE ALLPLAYERS set X = '"+x+"' where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);;

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Change Player X");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}
	public void changeY(int y, int pid){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE ALLPLAYERS set Y = '"+y+"' where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);;

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Change Player Y");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public void changePlayerLocation(String location, int pid){
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			String sql = "UPDATE ALLPLAYERS set Location = '"+location+"' where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);;

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in Change Player Location");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
	}

	public String getPlanetName01(int playerID){
		int mapID = getPlanetID(playerID);
		String ret = "null";
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT PlanetName01 FROM ALLPLANETS WHERE MapID = "+mapID+";" );
			while ( rs.next() ) {
				ret = rs.getString("PlanetName01");
			}
			rs.close();
			stmt.close();
			c.close();

			if(ret.equals("null")){
				System.out.println("getPlanetName01 returned Nothing");
			}
		} catch ( Exception e ) {
			System.out.println("Error in GetPlanetName01");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}

	public String getPlanetName02(int playerID){
		int mapID = getPlanetID(playerID);
		String ret = "null";
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT PlanetName02 FROM ALLPLANETS WHERE MapID = "+mapID+";" );
			while ( rs.next() ) {
				ret = rs.getString("PlanetName02");
			}
			rs.close();
			stmt.close();
			c.close();

			if(ret.equals("null")){
				System.out.println("getPlanetName02 returned Nothing");
			}
		} catch ( Exception e ) {
			System.out.println("Error in GetPlanetName02");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}

	private int getPlanetID(int playerID) {
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT MapID FROM ALLPLAYERS WHERE PlayerID = "+playerID+";" );
			while ( rs.next() ) {
				ret = rs.getInt("MapID");
			}
			rs.close();
			stmt.close();
			c.close();

			if(ret == -1){
				System.out.println("getPlanetID returned Nothing");
			}
		} catch ( Exception e ) {
			System.out.println("Error in GetPlanetID");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}

	public int getHouseX(int pid){
		int mid = getPlanetID(pid);
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT HouseX FROM ALLPLANETS WHERE MapID = "+mid+";" );
			while ( rs.next() ) {
				ret = rs.getInt("HouseX");
				System.out.println("House X: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in get House X");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
	public int getHouseY(int pid){
		int mid = getPlanetID(pid);
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT HouseY FROM ALLPLANETS WHERE MapID = "+mid+";" );
			while ( rs.next() ) {
				ret = rs.getInt("HouseY");
				System.out.println("House Y: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in get House Y");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
	public int getSID(int pid) {
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT SessionID FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
			while ( rs.next() ) {
				ret = rs.getInt("SessionID");
				System.out.println("Session ID: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("GetSID returned no session");
			}
		} catch ( Exception e ) {
			System.out.println("Error in get SID");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
	public int getPID(String handle) {
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT PlayerID FROM ALLPLAYERS WHERE Handle = '"+handle+"';" );
			while ( rs.next() ) {
				ret = rs.getInt("PlayerID");
				System.out.println("Player ID: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("GetPID returned no ID");
			}
		} catch ( Exception e ) {
			System.out.println("Error in get PID");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
	public void setServer(int pid, int serverID) {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);
			// set server in client
			stmt = c.createStatement();
			String sql = "UPDATE ALLPLAYERS set Server = "+serverID+" where PlayerID = "+pid+";";
			stmt.executeUpdate(sql);
			System.out.println("Server set as: " + serverID);
			// set client in server
			stmt = c.createStatement();
			sql = "UPDATE ALLPLAYERS set Client = "+pid+" where PlayerID = "+serverID+";";
			stmt.executeUpdate(sql);
			System.out.println("Client set as: " + pid);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.out.println("Error in SetServer");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}

	}
	public int getServer(int pid) {
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Server FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
			while ( rs.next() ) {
				ret = rs.getInt("Server");
				System.out.println("Server ID: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("GetServer returned no ID");
			}
		} catch ( Exception e ) {
			System.out.println("Error in getServer");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
	public int getClient(int pid) {
		int ret = -1;
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Client FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
			while ( rs.next() ) {
				ret = rs.getInt("Client");
				System.out.println("Client ID: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret == -1){
				System.out.println("GetClient returned no ID");
			}
		} catch ( Exception e ) {
			System.out.println("Error in getClient");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
	public String getHandle(int pid) {
		String ret = "none";
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection(databaseLoc);
			c.setAutoCommit(false);

			stmt = c.createStatement();
			ResultSet rs = stmt.executeQuery( "SELECT Handle FROM ALLPLAYERS WHERE PlayerID = "+pid+";" );
			while ( rs.next() ) {
				ret = rs.getString("Handle");
				System.out.println("Handle: "+ ret);
			}
			rs.close();
			stmt.close();
			c.close();
			if(ret.equals("none")){
				System.out.println("GetHandle returned nothing");
			}
		} catch ( Exception e ) {
			System.out.println("Error in getHandle");
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		return ret;	
	}
}
