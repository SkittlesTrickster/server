package databaseFunctions;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public final class CreateDatabase {
	public CreateDatabase(){
		// INITIALIZE DATABASE
		createDatabase();
		storeBasicItems();
		System.out.println("Database Created.");
	}

	public void createDatabase(){
		// create Database
		Connection c = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Created skaia.db successfully");
		// create Tables
		c = null; // Might not need
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			//System.out.println("Opened database successfully");
			// PLAYERS
			stmt = c.createStatement();
			String sql = "CREATE TABLE ALLPLAYERS " +
					"(PlayerID 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" MapID        INT     NOT NULL, " +  // This is the planet
					" SessionID       INT	  NOT NULL, " + 
					" Handle          TEXT    NOT NULL, " + 
					" Name            TEXT    NOT NULL, " + 
					" Password        TEXT    NOT NULL, " + 
					" Gender          TEXT    NOT NULL, " + 
					" Rung            INT     NOT NULL, " + 
					" Boondollars     INT     NOT NULL, " + 
					" Class           TEXT    NOT NULL, " + 
					" Aspect          TEXT    NOT NULL, " + 
					" Location        TEXT    NOT NULL, " +
					" HouseID         INT	  NOT NULL, " +
					" X            	  INT    NOT NULL, " + 
					" Y            	  INT    NOT NULL, " + 
					" Specibus01      TEXT   NOT NULL, " + 
					" Specibus02      TEXT   NOT NULL, " +
					" Specibus03      TEXT   NOT NULL, " +
					" Server          INT   NOT NULL, " +
					" Client          INT   NOT NULL, " +
					" Dreamer         TEXT	NOT NULL, " +
					" RungName      TEXT	NOT NULL, " +
					" HP          	INT		NOT NULL)"; // Needs CruxiteName
			stmt.executeUpdate(sql);

			// GLOBAL ITEMS
			stmt = c.createStatement();
			sql = "CREATE TABLE GLOBALITEMS " +
					"(ItemID 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" ItemName   	 TEXT    NOT NULL, " +
					" Pickup    	 TEXT    NOT NULL, " + 
					" UseType   	 TEXT	 NOT NULL, " +
					" ItemType	 	 TEXT    NOT NULL, " +
					" LocType01		 TEXT    NOT NULL, " +
					" LocType02		 TEXT	 NOT NULL," +
					" LocType03	 	 TEXT	 NOT NULL," +
					" Description    TEXT    NOT NULL)"; 
			stmt.executeUpdate(sql);

			// PLAYER INVENTORIES
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLINVENTORIES " +
					"(ItemKey 		INTEGER PRIMARY KEY AUTOINCREMENT," +
					" PlayerID 		INT		NOT NULL," +
					" ItemID          INT    NOT NULL)"; 
			stmt.executeUpdate(sql);

			// WEAPON STATS // Unused
			stmt = c.createStatement();
			sql = "CREATE TABLE WEAPONS " +
					"(ItemID 		INT		NOT NULL," +
					" Kind01 		TEXT		NOT NULL," + // scissors
					" Kind02 		TEXT		NOT NULL," +
					" Kind03 		TEXT		NOT NULL," +
					" WeaponCat 	TEXT		NOT NULL," + // polarm, area of attack, projectile
					" Effect	 	TEXT		NOT NULL," + // damage over time, cannot move
					" EffectChance	INT		NOT NULL," + // 10 (10%)
					" Damage 		INT		NOT NULL," +
					" Defence		INT		NOT NULL," +
					" Speed         INT     NOT NULL)"; 
			stmt.executeUpdate(sql);

			// CONTAINED ITEMS // Unused // Fill on individual Container Creation
			stmt = c.createStatement();
			sql = "CREATE TABLE CONTAINEDITEMS " +
					"(Key 		INTEGER PRIMARY KEY AUTOINCREMENT," +
					" ContainerID 		INT		NOT NULL," +
					" ItemID          INT    NOT NULL)"; 
			stmt.executeUpdate(sql);

			// BUFFING ITEMS // Unused
			stmt = c.createStatement();
			sql = "CREATE TABLE BUFFITEMS " +
					"(ItemID 		INTEGER 	NOT NULL," +
					" ModStat 		TEXT		NOT NULL," +
					" Modifier      INTEGER        NOT NULL)"; 
			stmt.executeUpdate(sql);

			// EQUIPABLE ITEMS // Unused
			stmt = c.createStatement();
			sql = "CREATE TABLE EQUIPABLEITEMS " +
					"(ItemID 		INTEGER 	NOT NULL," +
					" EquipType 	TEXT		NOT NULL," +
					" Damage 		TEXT		NOT NULL," +
					" Defense 		TEXT		NOT NULL," +
					" Speed	        INTEGER         NOT NULL)"; 
			stmt.executeUpdate(sql);

			// CREATED ITEMS // Unused // Stores Which ID to create an instance of
			stmt = c.createStatement();
			sql = "CREATE TABLE CREATEDITEMS " +
					"(ItemID 		INTEGER 	NOT NULL," +
					" CreatedID01 	INTEGER		NOT NULL," +
					" CreatedID02 	INTEGER		NOT NULL," +
					" CreatedID03 	INTEGER		NOT NULL)"; 
			stmt.executeUpdate(sql);

			// SESSIONS
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLSESSIONS " +
					"(SessionID 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" SessName           TEXT    NOT NULL, " + 
					" SessPassword       TEXT     NOT NULL)"; 
			stmt.executeUpdate(sql);

			// MAP IDS
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLMAPIDS " +
					"(MapID			INTEGER PRIMARY KEY 	AUTOINCREMENT," +
					" SessionID         TEXT	NOT NULL, " +
					" Location       	TEXT   	NOT NULL," +
					" Width       	INTEGER   	NOT NULL," +
					" Height       	INTEGER   	NOT NULL)"; 
			stmt.executeUpdate(sql);

			// PLANETS
			stmt = c.createStatement(); //(MapID,SessionID,PlanetName01,PlanetName02,Consorts,Gate01,Gate02,Gate03,Gate04,Gate05,Gate06,Gate07,Color01,Color02,Color03,Color04,Color05,WaterColor,HouseX,HouseY)
			sql = "CREATE TABLE ALLPLANETS " +
					"(PKID 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" MapID      	 INTEGER NOT NULL, " + 
					" SessionID      INTEGER NOT NULL, " + 
					" PlanetName01   TEXT    NOT NULL, " + 
					" PlanetName02   TEXT    NOT NULL, " +
					" Denizen        TEXT	 NOT NULL, " +
					" Consorts       TEXT    NOT NULL, " +
					" Gate01         TEXT	 NOT NULL, " + // No gates assigned
					" Gate02         TEXT 	 NOT NULL, " +
					" Gate03         TEXT	 NOT NULL, " +
					" Gate04         TEXT	 NOT NULL, " +
					" Gate05         TEXT	 NOT NULL, " +
					" Gate06         TEXT	 NOT NULL, " +
					" Gate07         TEXT	 NOT NULL, " +
					" Color01   	 TEXT     NOT NULL, " + //
					" Color02   	 TEXT     NOT NULL, " + 
					" Color03   	 TEXT     NOT NULL, " +
					" Color04   	 TEXT     NOT NULL, " + 
					" Color05   	 TEXT     NOT NULL, " + 
					" WaterColor   	  TEXT    NOT NULL, " +
					" HouseX          INT    NOT NULL, " +
					" HouseY          INT     NOT NULL)"; 
			stmt.executeUpdate(sql);


			// MAP ALT
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLROOMTYPES " +
					"(MapID 	 INT		NOT NULL," +
					" X      INT    NOT NULL, " + 
					" Y      INT     NOT NULL," + 
					" ALT      INT     NOT NULL)"; 
			stmt.executeUpdate(sql);
			// MAP LEAVE
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLMAPLEAVE " +
					"(MapID 	INT		NOT NULL," +
					" X      	INT    NOT NULL, " + 
					" Y      	INT     NOT NULL," + 
					" LEAVETO   TEXT     NOT NULL)"; 
			stmt.executeUpdate(sql);
			// MAP ITEMS
			stmt = c.createStatement();
			sql = "CREATE TABLE ALLMAPITEMS " +
					"(ItemKey 	INTEGER PRIMARY KEY     AUTOINCREMENT," +
					" MapID 	INT		NOT NULL," +
					" X      	INT     NOT NULL, " + 
					" Y      	INT     NOT NULL," + 
					" ItemID    INT     NOT NULL," + 
					" QTY      	INT     NOT NULL)"; 
			stmt.executeUpdate(sql);

			// Booleans, Triggers, and Waiting for sorting
			stmt = c.createStatement();
			sql = "CREATE TABLE TRIGGERS " + // Unused // Assign on Player Creation
					"(TID 	INTEGER PRIMARY KEY AUTOINCREMENT," + // PRIMARY KEY AUTOINCREMENT    "Simply don't provide data for the autoincrement column"
					" PlayerID        INT     NOT NULL, " +
					" MeteorTimer     INT	  NOT NULL, " + 
					" EnteredMedium   TEXT    NOT NULL, " + 
					" SpriteReleased  TEXT    NOT NULL, " + 
					" SpriteLocation  TEXT    NOT NULL, " + 
					" SpriteName      TEXT     NOT NULL, " + // Sprite location stored as an item in a room // May need an NPC Table later
					" ProtoTypeCount  INT     NOT NULL," +
					" IsAwake         TEXT  	NOT NULL, " +
					" CardSet         TEXT  	NOT NULL, " +
					" AlchSet         TEXT  	NOT NULL, " +
					" CruxSet         TEXT  	NOT NULL, " +
					" LatheSet         TEXT  	NOT NULL, " +
					" CruxtruderOpened  TEXT		NOT NULL, " +
					" RunTimer  	    TEXT		NOT NULL, " +
					" AlchimeterUpgradeLv  INT    NOT NULL)";
			//(PlayerID,MeteorTimer,EnteredMedium,SpriteReleased,SpriteLocation,SpriteName,ProtoTypeCount,IsAwake,CardSet,AlchSet,CruxSet,LatheSet,AlchimeterUpgradeLv)
			stmt.executeUpdate(sql);

			// close
			stmt.close();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Tables created successfully");

	}

	private void storeBasicItems() {
		Connection c = null;
		Statement stmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:TESTSKAIA.db");
			c.setAutoCommit(false);
			//System.out.println("Opened database successfully");

			// 1
			stmt = c.createStatement();
			String sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'bed', 'Y', 'sleep', 'none', 'house', 'none', 'none', 'You can sleep here.');"; 
			stmt.executeUpdate(sql);
			// 2
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'rock', 'Y', 'none', 'none', 'natural', 'rocky', 'none', 'A rock you found laying around.');"; 
			stmt.executeUpdate(sql);
			// 3
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'lizard', 'Y', 'none', 'none', 'natural', 'none', 'none', 'A small reptile.');"; 
			stmt.executeUpdate(sql);
			// 4
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'tree', 'N', 'none', 'none', 'natural', 'plant', 'none', 'A large leafy plant.');"; 
			stmt.executeUpdate(sql);
			// 5
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'plant', 'Y', 'none', 'none', 'natural', 'plant', 'none', 'Any of a variety of small plants.');"; 
			stmt.executeUpdate(sql);
			// 6
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'tree stump', 'N', 'none', 'none', 'natural', 'plant', 'none', 'The remains of a large leafy plant minus the large leafy part.');"; 
			stmt.executeUpdate(sql);
			// 7
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'pillar', 'Y', 'none', 'none', 'archetectural', 'ruin', 'none', 'A large structural object. You can barely fit it into your sylladex.');"; 
			stmt.executeUpdate(sql);
			// 8
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'desk', 'N', 'container', 'none', 'bedroom', 'office', 'none', 'A table with drawers.');"; 
			stmt.executeUpdate(sql);

			// ----------------------------------------------------------------------------------------------------------
			// Game Items
			// ---------------------------
			// Alchemiter
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'alchemiter', 'N', 'alchemiter', 'none', 'lab', 'none', 'none', 'Uses a todem to create an item.');"; 
			stmt.executeUpdate(sql);
			// Totem Lathe
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'totem lathe', 'N', 'totem lathe', 'none', 'lab', 'none', 'none', 'Creates a totem from a punched card.');"; 
			stmt.executeUpdate(sql);
			// Pre-Punched Card
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'pre-punched card', 'N', 'pre-punched card', 'none', 'none', 'none', 'none', 'Creates a totem from a punched card.');"; 
			stmt.executeUpdate(sql);
			// Cruxite Artifact
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'cruxite artifact', 'Y', 'entermedium', 'none', 'lab', 'none', 'none', 'A mysterious item.');"; 
			stmt.executeUpdate(sql);
			// Cruxite
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'cruxite', 'Y', 'none', 'none', 'lab', 'none', 'none', 'The Totem Lathe can turn this into a totem.');"; 
			stmt.executeUpdate(sql);
			// Totem
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'totem', 'Y', 'none', 'none', 'none', 'none', 'none', 'Used by the Alchimeter to create items.');"; 
			stmt.executeUpdate(sql);
			// Cruxtruder
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'cruxtruder', 'N', 'cruxtruder', 'none', 'lab', 'none', 'none', 'The Totem Lathe can turn this into a totem.');"; 
			stmt.executeUpdate(sql);
			// 16 SburbCD
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'sburbcd', 'Y', 'sburbcd', 'none', 'lab', 'none', 'none', 'The Totem Lathe can turn this into a totem.');"; 
			stmt.executeUpdate(sql);
			// ----------------------------------------------------------------------------------------------------------
			// 
			stmt = c.createStatement();
			sql = "INSERT INTO GLOBALITEMS (ItemID,ItemName,Pickup,UseType,ItemType,LocType01,LocType02,LocType03,Description) " +
					"VALUES (null, 'computer', 'Y', 'computer', 'none', 'lab', 'technology', 'machine', 'The Totem Lathe can turn this into a totem.');"; 
			stmt.executeUpdate(sql);

			stmt.close();
			c.commit();
			c.close();
		} catch ( Exception e ) {
			System.err.println( e.getClass().getName() + ": " + e.getMessage() );
			System.exit(0);
		}
		System.out.println("Item records created successfully");

	}

}
