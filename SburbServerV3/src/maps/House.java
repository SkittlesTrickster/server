package maps;

import generation.GenItems;

import java.util.ArrayList;
import java.util.Random;

public class House {
	Random rand = new Random();
	GenItems gi;
	Map m = new Map();
	private int hx = 0;
	private int hy = 0;
	
	public House(GenItems g){
		gi = g;
	}

	public Map returnMap(){
		System.out.println("House makemap started");

		// Roomsize is placeholder
		System.out.print("RoomSize: ");
		m.setDimensions(12,1);
		for(int i = 0;i<12;){
			m.setRoomType(i, 0, 1);
			System.out.print(" ("+i+",0) "+ m.getRoomType(i, 0) +" ");
			i++;
		}
		System.out.print('\n');

		for(int x = 0;x < 11;){
			for(int y = 0; y<1;){
				System.out.print(" ("+x+","+y+") "+ m.getRoomType(x, y) +" ");
				y++;
			}
			x++;
		}
		System.out.print('\n');
		// IDlist needs work

		// contains all ys > Containing all xs > containing all items

		//// ITEMS
		//Bedroom
		ArrayList<Integer> bedroomI = new ArrayList<Integer>();
		bedroomI.add(gi.getItemID("bed"));
		bedroomI.add(gi.getItemID("computer"));
		//Bathroom	
		ArrayList<Integer> bathroomI = new ArrayList<Integer>();
		bathroomI.add(1);
		//USHall
		ArrayList<Integer> ushallI = new ArrayList<Integer>();
		ushallI.add(1);
		//DSHall		
		ArrayList<Integer> dshallI = new ArrayList<Integer>();
		dshallI.add(1);
		//Livingroom		
		ArrayList<Integer> livingroomI = new ArrayList<Integer>();
		livingroomI.add(1);
		//Kitchen		
		ArrayList<Integer> kitchenI = new ArrayList<Integer>();
		kitchenI.add(1);
		//Parentsroom		
		ArrayList<Integer> parentsI = new ArrayList<Integer>();
		parentsI.add(1);
		//Attic		
		ArrayList<Integer> atticI = new ArrayList<Integer>();
		atticI.add(1);
		//Roof		
		ArrayList<Integer> roofI = new ArrayList<Integer>();
		roofI.add(1);
		//FrontYard		
		ArrayList<Integer> fyardI = new ArrayList<Integer>();
		fyardI.add(1);
		//BackYard		
		ArrayList<Integer> byardI = new ArrayList<Integer>();
		byardI.add(1);
		//Garage		
		ArrayList<Integer> garageI = new ArrayList<Integer>();
		garageI.add(1);

		// Sburbcd
		int room = rand.nextInt(12); //0-11
		if(room == 0){bedroomI.add(16);}
		else if(room == 1){bathroomI.add(16);}
		else if(room == 2){ushallI.add(16);}
		else if(room == 3){dshallI.add(16);}
		else if(room == 4){livingroomI.add(16);}
		else if(room == 5){kitchenI.add(16);}
		else if(room == 6){parentsI.add(16);}
		else if(room == 7){atticI.add(16);}
		else if(room == 8){roofI.add(16);}
		else if(room == 9){fyardI.add(16);}
		else if(room == 10){byardI.add(16);}
		else if(room == 11){garageI.add(16);}

		// Add all Room ItemID arrays to a single Array
		ArrayList<ArrayList<Integer>> items = new ArrayList<ArrayList<Integer>>();
		items.add(bedroomI);
		items.add(bathroomI);
		items.add(ushallI);
		items.add(dshallI);
		items.add(livingroomI);
		items.add(kitchenI);
		items.add(parentsI);
		items.add(atticI);
		items.add(roofI);
		items.add(fyardI);
		items.add(byardI);
		items.add(garageI);
		// Add Room ItemID array to ItemID array
		m.addItemIDs(items);

		System.out.println("HouseItems Work");
		//// LEAVE
		//Bedroom
		ArrayList<String> bedroomL = new ArrayList<String>();
		bedroomL.add("hallway");
		//Bathroom
		ArrayList<String> bathroomL = new ArrayList<String>();
		bathroomL.add("hallway");
		//USHall
		ArrayList<String> ushallL = new ArrayList<String>();
		ushallL.add("bedroom");
		ushallL.add("bathroom");
		ushallL.add("downstairs");
		ushallL.add("attic");
		//DSHall
		ArrayList<String> dshallL = new ArrayList<String>();
		dshallL.add("livingroom");
		dshallL.add("kitchen");
		dshallL.add("parents room");
		dshallL.add("garage");
		//Livingroom
		ArrayList<String> livingroomL = new ArrayList<String>();
		livingroomL.add("upstairs");
		livingroomL.add("hallway");
		livingroomL.add("kitchen");
		livingroomL.add("outside");
		//Kitchen
		ArrayList<String> kitchenL = new ArrayList<String>();
		kitchenL.add("hallway");
		kitchenL.add("livingroom");
		kitchenL.add("back yard");
		//Parentsroom
		ArrayList<String> parentsL = new ArrayList<String>();
		parentsL.add("hallway");
		//Attic
		ArrayList<String> atticL = new ArrayList<String>();
		atticL.add("hallway");
		atticL.add("roof");
		//Roof
		ArrayList<String> roofL = new ArrayList<String>();
		roofL.add("attic");
		//FrontYard
		ArrayList<String> fyardL = new ArrayList<String>();
		fyardL.add("inside");
		//BackYard
		ArrayList<String> byardL = new ArrayList<String>();
		byardL.add("inside");
		//Garage
		ArrayList<String> garageL = new ArrayList<String>();
		garageL.add("hallway");
		garageL.add("front yard");

		// Add all rooms to a single array
		ArrayList<ArrayList<String>> leave = new ArrayList<ArrayList<String>>();
		leave.add(bedroomL);
		leave.add(bathroomL);
		leave.add(ushallL);
		leave.add(dshallL);
		leave.add(livingroomL);
		leave.add(kitchenL);
		leave.add(parentsL);
		leave.add(atticL);
		leave.add(roofL);
		leave.add(fyardL);
		leave.add(byardL);
		leave.add(garageL);
		// Add RoomArray into Leave Array
		m.addLeave(leave);
		System.out.println("HouseLeave Finished");
		return m;

	}
	
	public int getHX(){return hx;}
	public int getHY(){return hy;}

}
