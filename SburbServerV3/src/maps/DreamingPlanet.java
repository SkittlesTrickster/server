package maps;

import generation.GenItems;

import java.util.ArrayList;
import java.util.Random;

public class DreamingPlanet {
	
	Map planet = new Map();
	//TODO Map moon = new Map();
	//TODO Map tower = new Map();
	Random rand = new Random();
	GenItems g;
	
	public DreamingPlanet(GenItems gi){
		g = gi;
	}
	
	public Map returnMap(String whichMap){
		planet.setDimensions(10,10);
		g.getIDlist("natural"); //TODO set according to pros/ders

		// Derspit
		int shop = 49;
		int castle = 50;
		int road = 51;
		int houses = 52;
		int fountain = 53;
		int chain = 54;

		// Set Types
		if(whichMap.equals("prospit")){
			shop = 37;
			castle = 38;
			road = 39;
			houses = 40;
			fountain = 41;
			chain = 42;
		}
		if(whichMap.equals("derse")){
			shop = 43;
			castle = 44;
			road = 45;
			houses = 46;
			fountain = 47;
			chain = 43;
		}

		// START ITERATING THROUGH THE ROOMS
		for(int y=0;y<10;){
			ArrayList<ArrayList<String>> listOfY_leave = new ArrayList<ArrayList<String>>();
			ArrayList<ArrayList<Integer>> listOfY_items = new ArrayList<ArrayList<Integer>>();

			for(int x=0; x<10;){
				// LEAVE  // For each x create an x array of leaves
				ArrayList<String> listOfX_leave = new ArrayList<String>(); // contains strings
				listOfX_leave.add("north");
				listOfX_leave.add("south");
				listOfX_leave.add("east");
				listOfX_leave.add("west");

				// ITEMS
				ArrayList<Integer> listOfX_items = new ArrayList<Integer>(); // contains IDs
				int numItems = rand.nextInt(6);
				for(int i = 0; i <= numItems;){
					listOfX_items.add(g.getID((rand.nextInt(g.getSize()))));
					i++;
				}

				// Iterators
				int houseItr = 1;
				int shopItr = 1;
				//Num controls where room types are set
				int num = (y*10)+x;
				if(num == 0){
					// chain
					planet.setRoomType(x, y, chain);
					System.out.print("-");
				}
				else if(num == 3 || num == 4 || num == 8 || num == 9 || num == 10 || num == 11 || num == 20 || num == 21 || num == 23 || num == 24 || 
						num == 28 || num == 29 || num == 33 || num == 34 || num == 38 || num == 39 || num == 60 || num == 61 || num == 70 ||
						num == 71 || num == 73 || num == 74 || num == 78 || num == 79 || num == 83 || num == 84 || num == 88 || num == 89 || 
						num == 90 || num == 91){
					// houses
					planet.setRoomType(x, y, houses);	
					//TODO need to generate house contents here
					// leaveto house
					listOfX_leave.add("house"+houseItr);
					houseItr++;
					System.out.print("H");
				}
				else if(num == 26 || num == 36 || num == 40 || num == 41 || num == 53 || num == 54 || num == 58 || num == 59 || num == 76 
						|| num == 86){
					// shops
					planet.setRoomType(x, y, shop);
					//TODO Generate shop items
					// leaveto shop
					listOfX_leave.add("shop"+shopItr);
					shopItr++;
					System.out.print("S");
				}
				else if(num == 56){
					// castle
					planet.setRoomType(x, y, castle);	
					// leaveto castle
					listOfX_leave.add("castle");
					System.out.print("W");
				}
				else if(num == 6){
					// fountain
					planet.setRoomType(x, y, fountain);
					System.out.print("T");
				}
				else {
					// road
					planet.setRoomType(x, y, road);
					System.out.print("#");
				}
				// add leave x to y
				listOfY_leave.add(listOfX_leave);
				listOfY_items.add(listOfX_items);

				// iterate
				x++;
			}
			// add y to main array
			planet.addLeave(listOfY_leave);
			planet.addItemIDs(listOfY_items);
			System.out.print('\n');
			// iterate
			y++;
		}
		return planet;
	}

}
