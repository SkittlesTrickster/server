package maps;

import java.util.ArrayList;

public class Map {
	private int[][] roomType;
	private ArrayList<ArrayList<ArrayList<String>>> leave;
	private ArrayList<ArrayList<ArrayList<Integer>>> itemIDs;
	private int xLimit = -1;
	private int yLimit = -1;
	
	public int[][] getRoomTypeArray() {
		return roomType;
	}
	public void setRoomTypeArray(int[][] roomType) {
		this.roomType = roomType;
	}
	public void setRoomType(int x, int y, int value) {
		roomType[x][y] = value;
	}
	public int getRoomType(int x, int y) {
		return roomType[x][y];
	}
	
	public ArrayList<ArrayList<ArrayList<String>>> getLeaveArray() {
		return leave;
	}
	public void setLeaveArray(ArrayList<ArrayList<ArrayList<String>>> leave) {
		this.leave = leave;
	}
	public void addLeave(ArrayList<ArrayList<String>> leaveY) {
		leave.add(leaveY);
	}
	public ArrayList<ArrayList<ArrayList<Integer>>> getItemIDsArray() {
		return itemIDs;
	}
	public void setItemID(ArrayList<ArrayList<ArrayList<Integer>>> itemIDs) {
		this.itemIDs = itemIDs;
	}
	public void addItemIDs(ArrayList<ArrayList<Integer>> itemsY) {
		itemIDs.add(itemsY);
	}
	
	public int getxLimit(){return xLimit;}
	public int getyLimit(){return yLimit;}
	
	public void setDimensions(int w, int h){
		// w and h are total size 20 ... array runs 0 - 19
		xLimit = w;
		yLimit = h;
		System.out.println("Initiating map " + w + " wide and " + h + "high");
		roomType = new int[w][h]; // indexes start at zero but w and h should be dimensions
		leave = new ArrayList<ArrayList<ArrayList<String>>>();
		itemIDs = new ArrayList<ArrayList<ArrayList<Integer>>>();
		System.out.println("RoomSize Finished");
	}
	
	
	
	

}
