package maps;

import generation.GenItems;

import java.util.ArrayList;
import java.util.Random;

public class Planet {
	GenItems gi;
	Map m = new Map();
	Random rand = new Random();
	int hx;
	int hy;
	
	public Planet(GenItems g){
		gi = g;
	}
	
	public Map returnMap(){

		// Initiate Arrays
		m.setDimensions(20,20); // planet will always be 20x20
		gi.getIDlist("natural");

		// GENERATE ALTITUDE
		//// for every other column
		for(int y=0; y< 20;){
			for(int x=0; x<20;){
				m.setRoomType(x, y, planetAlt(x,y)); //Random Number
				x++; // 0,2,4,6,8,10,12,14,16,18
			}
			y = y+2;
		}
		//// the rest
		for(int y=1; y< 20;){
			for(int x=0; x<20;){
				// average the values on either side
				m.setRoomType(x, y, meanAlt(x,y));
				x++; // 1,3,5,7,9,11,13,15,17,19
			}
			y = y+2;
		}
		//Display
		for(int y=0; y<20;){
			for(int x=0; x<20;){
				//System.out.print(roomType[x][y] + " ");
				x++;
			}
			//System.out.print('\n');
			y++;
		}
		System.out.println("Successfully Generated Altitude");

		// Generate House Location
		// gen y
		while(m.getRoomType(hx, hy) == 0){
			System.out.println("hx and hy in Planet: " + hx + " " + hy);
			hy = rand.nextInt(20);
			hx = rand.nextInt(20);
			System.out.println("hx and hy in Planet: " + hx + " " + hy);
		}

		// GENERATE LEAVE
		for(int y=0; y< 20;){
			// Columns
			ArrayList<ArrayList<String>> listOfY = new ArrayList<ArrayList<String>>(); // contains y
			for(int x=0; x<20;){
				// Rows
				ArrayList<String> listOfX = new ArrayList<String>(); // contains strings
				listOfX.add("north");
				listOfX.add("south");
				listOfX.add("east");
				listOfX.add("west");
				if(x == hx && y == hy){
					listOfX.add("house");
				}
				// Add Y/Strings to x
				listOfY.add(listOfX);

				// leave is the x coordinate list(20 big)
				//20 templists are added to a single leave list acting as the y
				x++;
			}
			//System.out.println("Successful y"+x);
			m.addLeave(listOfY);
			y++;
		}
		System.out.println("Successfully Generated Leave");

		// GENERATE ITEMS
		for(int y=0; y< 20;){
			// COLUMN Y
			ArrayList<ArrayList<Integer>> listOfY = new ArrayList<ArrayList<Integer>>(); // contains y
			for(int x=0; x<20;){
				// ROW X
				ArrayList<Integer> listOfX = new ArrayList<Integer>(); // contains IDs
				int numItems = rand.nextInt(6);
				for(int i = 0; i <= numItems;){
					System.out.print("Get size returns: ");
					System.out.println(gi.getSize());
					listOfX.add(gi.getID((rand.nextInt(gi.getSize()))));
					i++;
				}
				// ADD ROW X TO COLUMN Y
				listOfY.add(listOfX);
				x++;
			}
			// ADD Y COLUMN CONTAINING X ENTRIES TO ITEMID ARRAY
			m.addItemIDs(listOfY);
			y++;
		}
		System.out.println("Successfully Generated ItemID array");
		// Create String of Names from IDs every room load
		
		return m;

	}
	
	private int planetAlt(int x, int y) {
		int r = rand.nextInt(3);
		int alt = 0;
		// 0 stays the same
		if(r == 0){
			// 0 stays the same	
		}
		else if(r == 1){ // up
			if(alt + 1 < 6){
				alt++;
			}
			else{ // if it can't go up
				if(rand.nextBoolean()){
					alt--; // go down
				}
				// else stay the same
			}
		}
		else if(r == 2){ // down
			if(alt - 1 > -1){
				alt--;
			}
			else{ // if it can't go down
				if(rand.nextBoolean()){
					alt++; // go up
				}
				// else stay the same
			}
		}
		else {
			System.out.println("Room Altitude Error");
		}
		//System.out.println("Alt: " + alt);
		return alt;
	}

	private int meanAlt(int x, int y){
		//System.out.println("x: " + x + " y: "+y);
		int miny;
		int plsy;
		int meanAlt;
		// Above this
		if(y-1 > -1){
			miny = y-1;}
		else{
			miny = 19;	}
		// Below this
		if(y+1 < 20){
			plsy = y+1;}
		else{
			plsy = 0;}
		// Mean altitudes at position
		int added = m.getRoomType(x, miny)+m.getRoomType(x, plsy);
		meanAlt = added/2;
		//System.out.println("MeanAlt:" + meanAlt);

		// get values to the left and right
		return meanAlt;
	}

}
