package maps;

import generation.GenItems;

import java.util.ArrayList;
import java.util.Random;

public class Skaia {
	Random rand = new Random();
	Map m = new Map();
	GenItems g;
	
	public Skaia(GenItems gi){
		g = gi;
	}
	
	public Map returnMap(){
		m.setDimensions(10,10);
		g.getIDlist("natural");
		// Room Types // 12 black // 13 white
		// Squares
		Boolean black = true;
		for(int y=0; y<10;){
			for(int x=0; x<10;){
				if(black == true){
					m.setRoomType(x, y, 12);
					black = false;
					System.out.print("#");
				}
				else{
					m.setRoomType(x, y, 13);
					black = true;
					System.out.print(".");
				}
				x++;
			}
			if(black == true){
				black = false;
			}
			else{
				black = true;
			}
			System.out.print('\n');
			y++;
		}
		// Prospit Base
		m.setRoomType(2, 2, 15);
		// Derse Base
		m.setRoomType(7, 7, 14);

		// Leave
		for(int y=0; y< 20;){
			// Columns
			ArrayList<ArrayList<String>> listOfY = new ArrayList<ArrayList<String>>(); // contains y
			for(int x=0; x<20;){
				// Rows
				ArrayList<String> listOfX = new ArrayList<String>(); // contains strings
				listOfX.add("north");
				listOfX.add("south");
				listOfX.add("east");
				listOfX.add("west");
				if(x == 2 && y == 2){
					listOfX.add("prospit base");
				}
				if(x == 7 && y == 7){
					listOfX.add("derse base");
				}
				// Add Y/Strings to x
				listOfY.add(listOfX);

				// leave is the x coordinate list(20 big)
				//20 templists are added to a single leave list acting as the y
				x++;
			}
			//System.out.println("Successful y"+x);
			m.addLeave(listOfY);
			y++;
		}
		//System.out.println("Successfully Generated Leave");


		// Items
		for(int y=0; y< 20;){
			// COLUMN Y
			ArrayList<ArrayList<Integer>> listOfY = new ArrayList<ArrayList<Integer>>(); // contains y
			for(int x=0; x<20;){
				// ROW X
				ArrayList<Integer> listOfX = new ArrayList<Integer>(); // contains IDs
				int numItems = rand.nextInt(6);
				for(int i = 0; i <= numItems;){
					listOfX.add(g.getID((rand.nextInt(g.getSize()))));
					i++;
				}
				// ADD ROW X TO COLUMN Y
				listOfY.add(listOfX);
				x++;
			}
			// ADD Y COLUMN CONTAINING X ENTRIES TO ITEMID ARRAY
			m.addItemIDs(listOfY);
			y++;
		}
		System.out.println("Successfully Generated ItemID array");
		return m;
	}
}
