package maps;

import generation.GenItems;

import java.util.ArrayList;
import java.util.Random;

public class RandomDungeon {
	Random rand = new Random();
	Map m = new Map();
	int mapW = 20;
	int mapH = 20;
	int mapMax = mapW * mapH;
	int startPos = 210;
	int dir;
	ArrayList<Integer> occupiedRooms;
	ArrayList<Integer> possibleLocs;
	int[][] finalMap = new int[20][20];
	
	GenItems g;

	public RandomDungeon(GenItems gi){
		g = gi;
	}
	
	public Map returnMap(){

		m.setRoomTypeArray(generateTunnelSystem());
		m.setDimensions(20, 20);
		g.getIDlist("natural");

		for(int y=0;y<10;){
			// Init y arrays
			ArrayList<ArrayList<String>> listOfY_leave = new ArrayList<ArrayList<String>>();
			ArrayList<ArrayList<Integer>> listOfY_items = new ArrayList<ArrayList<Integer>>();

			for(int x=0; x<10;){
				if(m.getRoomType(x, y) != -1){ // If room exists
					// Init x arrays
					ArrayList<String> listOfX_leave = new ArrayList<String>(); // contains strings
					ArrayList<Integer> listOfX_items = new ArrayList<Integer>();

					// TODO Assign ITEMS
					//generateRandomItems();
					listOfX_items.add(g.getID((rand.nextInt(g.getSize()))));

					// TODO assign leave
					dungeonLeave(listOfX_leave,x,y);
					if(!listOfX_leave.isEmpty()){
						System.out.println("ListX contains: " + listOfX_leave.get(0));
					}
					else{
						System.out.println("leave array is null");
					}

					// add leave x to y
					listOfY_leave.add(listOfX_leave);
					listOfY_items.add(listOfX_items);
				}

				// iterate
				x++;
			}
			// add y to main array
			if(!listOfY_leave.isEmpty()){
				m.addLeave(listOfY_leave);
				System.out.println("Added y's to leave");
			}
			else{
				System.out.println("y's were empty");
			}
			m.addItemIDs(listOfY_items);
			// iterate
			y++;
		}
		return m;
	}

	public int[][] generateTunnelSystem(){
		// Initiate
		occupiedRooms = new ArrayList<Integer>();
		possibleLocs = new ArrayList<Integer>();
		System.out.println("Empty: " + possibleLocs);

		// create first tunnel
		occupiedRooms.add(startPos); // Add first Location
		assignToXY(startPos,0);
		generateTunnel(startPos,50); //center
		
		// Create offshoots
		generateTunnel(occupiedRooms.get(rand.nextInt(occupiedRooms.size())),3); // random tunnel off of an existing tunnel
		generateTunnel(occupiedRooms.get(rand.nextInt(occupiedRooms.size())),3);
		generateTunnel(occupiedRooms.get(rand.nextInt(occupiedRooms.size())),3);
		generateTunnel(occupiedRooms.get(rand.nextInt(occupiedRooms.size())),3);

		// check for longest segment leading to a 2 and assign the denizen chamber to it
		
		showMap();
		return finalMap;
	}
	
	public void assignToXY(int pos, int roomType){
		System.out.println("Pos in assignxy: " + pos);
		int tx = pos % 20; // remainder of dividing by 10
		int ty = ((pos % 400)-tx)/20;
		// TODO return map
		finalMap[tx][ty] = roomType; // assign basic room
	}

	public void generateTunnel(int pos, int tunnelLength){
		System.out.println("*** Start Tunnel "+ pos + " ***");
		// Check possibilites
		checkPossibilities(pos);
		// Reroll if no possibilities
		if(possibleLocs.isEmpty()){
			while(possibleLocs.isEmpty()){
				checkPossibilities(pos);
				pos = occupiedRooms.get(rand.nextInt(occupiedRooms.size()));
			}
		}

		for(int c = 0; c < tunnelLength;){
			System.out.println("Pos: " + pos);
			checkPossibilities(pos);
			if(!(possibleLocs.isEmpty())){
				// get a random index from possibilities
				int randomMove = possibleLocs.get(rand.nextInt(possibleLocs.size())); 
				System.out.println("Movement: " + randomMove);
				// increment position
				pos = pos+randomMove; 
				// add to Occupied List
				occupiedRooms.add(pos);
				assignToXY(pos,1);
				if(c+1 >= tunnelLength){
					System.out.println("!!! End of tunnel (max) "+pos+" !!!");
					assignToXY(pos,2);
				}
			}
			else{ // if possibilites are empty
				System.out.println("!!! End of tunnel (wall) "+pos+" !!!");
				assignToXY(pos,2); // end of tunnel
				break;
			}
			System.out.println("pos: " + pos);
			possibleLocs.clear();
			c++;
		}
	}

	private void checkPossibilities(int pos) {
		if(pos > (mapW-1) && countNeighbors(pos-mapW) <3 && !occupiedRooms.contains(pos-mapW)){
			possibleLocs.add(mapW*-1);
		}
		// Is South Clear?
		if(pos < (mapMax-mapW-1) && countNeighbors(pos+mapW) <3 && !occupiedRooms.contains(pos+mapW)){
			possibleLocs.add(mapW);
		}
		// Is West Clear?
		if((pos % mapW != 0) && countNeighbors(pos-1) <3 && !occupiedRooms.contains(pos-1)){
			possibleLocs.add(1*-1);
		}
		// Is East Clear?
		if((pos % mapW != (mapW-1)) && countNeighbors(pos+1) <3 && !occupiedRooms.contains(pos+1)){
			possibleLocs.add(1);
		}
		System.out.println("possibility: " + possibleLocs);
	}

	public void showMap(){
		for(int s = 0; s < mapW; s++){ // tens
			for(int l = 0; l < mapW; l++){ // ones
				//Print row of ten
				//System.out.print((s*mapW)+l +" "); // Testing display
				if((s*mapW)+l == 210){
					System.out.print("@ ");
				}
				else if(finalMap[l][s] == 2){
					System.out.print("! ");
				}
				else if(finalMap[l][s] == 1){
					System.out.print(". ");
				}
				else{
					System.out.print("# ");
				}
			}
			System.out.print('\n');
		}
	}

	public int countNeighbors(int tpos){
		int neighbors = 0;
		if(occupiedRooms.contains(tpos+1)){ // East
			neighbors++;
		}
		if(occupiedRooms.contains(tpos-1)){ // West
			neighbors++;
		}
		if(occupiedRooms.contains(tpos-mapW)){ // North
			neighbors++;
		}
		if(occupiedRooms.contains(tpos+mapW)){ // South
			neighbors++;
		}
		if(occupiedRooms.contains(tpos-mapW+1)){ // NE
			neighbors++;
		}
		if(occupiedRooms.contains(tpos-mapW-1)){ // NW
			neighbors++;
		}
		if(occupiedRooms.contains(tpos+mapW+1)){ // SE
			neighbors++;
		}
		if(occupiedRooms.contains(tpos+mapW-1)){ // SW
			neighbors++;
		}
		return neighbors;
	}
	
	public void dungeonLeave(ArrayList<String> leave, int curX, int curY){
		int north = curY -1;
		int south = curY +1;
		int east = curX + 1;
		int west = curX - 1;
		// nsew

		if(north >= 0){
			if(m.getRoomType(curX, north) != -1){
				leave.add("north");
			}
		}
		if(south < m.getyLimit()){
			if(m.getRoomType(curX, south) != -1){
				leave.add("south");
			}
		}
		if(east < m.getxLimit()){
			if(m.getRoomType(east, curY) != -1){
				leave.add("east");
			}
		}
		if(west >= 0){
			if(m.getRoomType(west, curY) != -1){
				leave.add("west");
			}
		}
	}

}
